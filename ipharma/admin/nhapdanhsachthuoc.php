<?php include './header.php'; ?>
<!-- Main Content -->
<section class="content home">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12">
                <h2>Nhập danh sách thuốc
                <small>Welcome to iPhama</small>
                </h2>
            </div>            
             <div class="col-lg-7 col-md-7 col-sm-12 text-right">
                <div class="inlineblock text-center m-r-15 m-l-15 hidden-md-down">
                    
                </div>
                <div class="inlineblock text-center m-r-15 m-l-15 hidden-md-down">
                   
                </div>
                <button class="btn btn-white btn-icon btn-round hidden-sm-down float-right m-l-10" type="button">
                    <i class="zmdi zmdi-plus"></i>
                </button>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-home"></i> iPharma</a></li>
                    <li class="breadcrumb-item active">Nhập danh sách thuốc</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
         <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                       
                        <ul class="header-dropdown">
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        
                        <form>
          <div class="row">
              <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                 <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Loại thuốc</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Loại thuốc">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Tên thuốc</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Tên thuốc">
                    </div>
                  </div>
                 <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Ngày nhập</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                       <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="zmdi zmdi-calendar"></i>
                                    </span>
                                    <input type="text" class="datetimepicker form-control" placeholder="Ngày nhập">
                                </div>   
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Ngày sản xuất</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                       <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="zmdi zmdi-calendar"></i>
                                    </span>
                                    <input type="text" class="datetimepicker form-control" placeholder="Ngày sản xuất">
                                </div>   
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Hạn sử dụng</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                       <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="zmdi zmdi-calendar"></i>
                                    </span>
                                    <input type="text" class="datetimepicker form-control" placeholder="Hạn sử dụng">
                                </div>   
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Chú ý</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Chú ý">
                    </div>
                  </div>
                   <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Giá thuốc</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Giá thuốc">
                    </div>
                  </div>
                   <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Chống chỉ định</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Chống chỉ định">
                    </div>
                  </div>
                  
                  
                  
              </div>
              <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                   <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Mã loại thuốc</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                       <input type="text" class="form-control" id="datepicker" placeholder="Mã loại thuốc">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Nhà cung cấp</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Nhà cung cấp">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Lô sản xuất</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                       <input type="text" class="form-control" id="datepicker" placeholder="Lô sản xuất">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Công dụng</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                       <input type="text" class="form-control" id="datepicker" placeholder="Công dụng">
                    </div>
                  </div>
                 <div class="form-group row">
                        <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Trạng thái</label>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;margin-top: 0px;">
                         <select class="show-tick">
                                    <option value="">Hoạt động</option>
                                    <option value="10">Không hoạt động</option>
                                    
                        </select>
                        </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Tác dụng phụ</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                       <input type="text" class="form-control" id="datepicker" placeholder="Tác dụng phụ">
                    </div>
                  </div>
                    <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Tác dụng</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                       <input type="text" class="form-control" id="datepicker" placeholder="Tác dụng">
                    </div>
                  </div>
              </div>
          </div>
        </form>
                        <div class="row clearfix">
                            <div class="col-3 col-md-3">
                                
                            </div>
                            <div class="col-3 col-md-3">
                                <button type="button" class="btn btn-primary"  data-whatever="@mdo">Tìm kiếm</button>
                            </div>
                            <div class="col-3 col-md-5">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#centralModalSm" data-whatever="@mdo">Nhập</button>
                            </div>

                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                         <ul class="header-dropdown">
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="tab-content m-t-10">
                            <div class="tab-pane table-responsive active" id="All">
                                
                                <table class="table table-hover">
                                    <thead>
                                        <tr>                                       
                                            <th>Loại thuốc</th>
                                            <th>Tên thuốc</th>
                                            <th>Ngày nhập</th>
                                            <th>Ngày sản xuất</th>
                                            <th>Hạn sử dụng</th>
                                            <th>Giá thuốc</th>
                                            <th>Chống chỉ định</th>
                                            <th>Mã loại thuốc</th>
                                            <th>Nhà cung cấp</th>
                                            <th>Lô sản xuất</th>
                                            <th>Công dụng</th>
                                            <th>Trạng thái</th>
                                            <th>Tác dụng phụ</th>
                                            <th>Tác dụng</th>
                                            <th>Chú ý</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>                                 
                                            <td>Loại thuốc 1</td>
                                            <td>Thuốc 1</td>
                                            <td>10/1/2019</td>
                                            <td>1/1/2019</td>
                                            <td>1/1/2020</td>
                                            <td>100.000 đ</td>
                                            <td>Chống chỉ định 1</td>
                                            <td>1</td>
                                            <td>Nhà cung cấp 1</td>
                                            <td>Lô 1</td>
                                            <td>Chữa cảm cúng</td>
                                            <td>Hoạt động</td>
                                            <td>Tác dụng phụ 1</td>
                                            <td>Tác dụng 1</td>
                                            <td>Bảo quản nơi thoáng mát</td>
                                        </tr>
                                        <tr>                                 
                                            <td>Loại thuốc 1</td>
                                            <td>Thuốc 1</td>
                                            <td>10/1/2019</td>
                                            <td>1/1/2019</td>
                                            <td>1/1/2020</td>
                                            <td>100.000 đ</td>
                                            <td>Chống chỉ định 1</td>
                                            <td>1</td>
                                            <td>Nhà cung cấp 1</td>
                                            <td>Lô 1</td>
                                            <td>Chữa cảm cúng</td>
                                            <td>Hoạt động</td>
                                            <td>Tác dụng phụ 1</td>
                                            <td>Tác dụng 1</td>
                                            <td>Bảo quản nơi thoáng mát</td>
                                        </tr>
                                        <tr>                                 
                                            <td>Loại thuốc 1</td>
                                            <td>Thuốc 1</td>
                                            <td>10/1/2019</td>
                                            <td>1/1/2019</td>
                                            <td>1/1/2020</td>
                                            <td>100.000 đ</td>
                                            <td>Chống chỉ định 1</td>
                                            <td>1</td>
                                            <td>Nhà cung cấp 1</td>
                                            <td>Lô 1</td>
                                            <td>Chữa cảm cúng</td>
                                            <td>Hoạt động</td>
                                            <td>Tác dụng phụ 1</td>
                                            <td>Tác dụng 1</td>
                                            <td>Bảo quản nơi thoáng mát</td>
                                        </tr>
                                        <tr>                                 
                                            <td>Loại thuốc 1</td>
                                            <td>Thuốc 1</td>
                                            <td>10/1/2019</td>
                                            <td>1/1/2019</td>
                                            <td>1/1/2020</td>
                                            <td>100.000 đ</td>
                                            <td>Chống chỉ định 1</td>
                                            <td>1</td>
                                            <td>Nhà cung cấp 1</td>
                                            <td>Lô 1</td>
                                            <td>Chữa cảm cúng</td>
                                            <td>Hoạt động</td>
                                            <td>Tác dụng phụ 1</td>
                                            <td>Tác dụng 1</td>
                                            <td>Bảo quản nơi thoáng mát</td>
                                        </tr>
                                         
                                        
                                    </tbody>
                                </table>                            
                            </div>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
                  
    </div>


</section>

<!-- Central Modal Small -->
<div class="modal fade" id="centralModalSm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Change class .modal-sm to change the size of the modal -->
  <div class="modal-dialog modal-lg" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Thêm mới</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form>
          <div class="row">
              <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                 <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Loại thuốc</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Loại thuốc">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Tên thuốc</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Tên thuốc">
                    </div>
                  </div>
                 <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Ngày nhập</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                       <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="zmdi zmdi-calendar"></i>
                                    </span>
                                    <input type="text" class="datetimepicker form-control" placeholder="Ngày nhập">
                                </div>   
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Ngày sản xuất</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                       <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="zmdi zmdi-calendar"></i>
                                    </span>
                                    <input type="text" class="datetimepicker form-control" placeholder="Ngày sản xuất">
                                </div>   
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Hạn sử dụng</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                       <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="zmdi zmdi-calendar"></i>
                                    </span>
                                    <input type="text" class="datetimepicker form-control" placeholder="Hạn sử dụng">
                                </div>   
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Chú ý</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Chú ý">
                    </div>
                  </div>
                   <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Giá thuốc</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Giá thuốc">
                    </div>
                  </div>
                   <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Chống chỉ định</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Chống chỉ định">
                    </div>
                  </div>
                  
                  
                  
              </div>
              <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                   <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Mã loại thuốc</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                       <input type="text" class="form-control" id="datepicker" placeholder="Mã loại thuốc">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Nhà cung cấp</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Nhà cung cấp">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Lô sản xuất</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                       <input type="text" class="form-control" id="datepicker" placeholder="Lô sản xuất">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Công dụng</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                       <input type="text" class="form-control" id="datepicker" placeholder="Công dụng">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Trạng thái</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                       <input type="text" class="form-control" id="datepicker" placeholder="Trạng thái">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Tác dụng phụ</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                       <input type="text" class="form-control" id="datepicker" placeholder="Tác dụng phụ">
                    </div>
                  </div>
                    <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Tác dụng</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                       <input type="text" class="form-control" id="datepicker" placeholder="Tác dụng">
                    </div>
                  </div>
              </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        
        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal">Thêm mới</button>
      </div>
    </div>
  </div>
</div>
<!-- Central Modal Small -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Thông báo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Thêm mới thành công
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>

<!-- Jquery Core Js --> 


<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Bootstrap JS and jQuery v3.2.1 -->
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- slimscroll, waves Scripts Plugin Js -->  

<script src="plugins/dropzone/dropzone.js"></script> <!-- Dropzone Plugin Js -->
<script src="plugins/momentjs/moment.js"></script> <!-- Moment Plugin Js -->
<script src="plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

<script src="assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js -->
<script>
    $(function () {
    //Datetimepicker plugin
    $('.datetimepicker').bootstrapMaterialDatePicker({
        format: 'dddd DD MMMM YYYY - HH:mm',
        clearButton: true,
        weekStart: 1
    });
});
</script>
</body>

<!-- Mirrored from thememakker.com/templates/oreo/hospital/html/light/index.php by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Apr 2019 15:00:40 GMT -->
</html>