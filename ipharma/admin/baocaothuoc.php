<?php include './header.php'; ?>
<!-- Main Content -->
<section class="content home">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12">
                <h2>Báo cáo thuốc
                <small>Welcome to iPhama</small>
                </h2>
            </div>            
            <div class="col-lg-7 col-md-7 col-sm-12 text-right">
                <div class="inlineblock text-center m-r-15 m-l-15 hidden-md-down">
                    
                </div>
                <div class="inlineblock text-center m-r-15 m-l-15 hidden-md-down">
                   
                </div>
                <button class="btn btn-white btn-icon btn-round hidden-sm-down float-right m-l-10" type="button">
                    <i class="zmdi zmdi-plus"></i>
                </button>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-home"></i> iPharma</a></li>
                    <li class="breadcrumb-item active">Báo cáo thuốc</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
         <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                       
                        <ul class="header-dropdown">
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div id="chartContainer" style="height: 300px; width: 100%;"></div>
                    </div>
                    
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                       
                        <ul class="header-dropdown">
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div id="chartContainer2" style="height: 300px; width: 100%;"></div>
                    </div>
                    
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                       
                        <ul class="header-dropdown">
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div id="chartContainer3" style="height: 300px; width: 100%;"></div>
                    </div>
                    
                </div>
            </div>
        </div>
                  
    </div>


</section>

<!-- Central Modal Small -->

<!-- Central Modal Small -->



<!-- Jquery Core Js --> 


<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Bootstrap JS and jQuery v3.2.1 -->
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- slimscroll, waves Scripts Plugin Js -->  

<script src="plugins/dropzone/dropzone.js"></script> <!-- Dropzone Plugin Js -->
<script src="plugins/momentjs/moment.js"></script> <!-- Moment Plugin Js -->
<script src="plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<script src="assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js -->
<script>
    $(function () {
    //Datetimepicker plugin
    $('.datetimepicker').bootstrapMaterialDatePicker({
        format: 'dddd DD MMMM YYYY - HH:mm',
        clearButton: true,
        weekStart: 1
    });
});
</script>
<script type="text/javascript">
    window.onload = function () {

        var chart = new CanvasJS.Chart("chartContainer", {
    animationEnabled: true,
    title:{
        text : "Doanh số",
        fontFamily: "tahoma",
        verticalAlign: "top",
        horizontalAlign: "left",    
    },  
    axisX:{
        labelFontSize: 10
      },
    axisY: {
        
        lineColor: "black",
        labelFontColor: "black",
        tickColor: "black",
        interval: 10000,
        maximum: 110000,
        minimum: 10000,
    },  
    toolTip: {
        shared: true
    },
    legend: {
        cursor:"pointer",
        itemclick: toggleDataSeries
    },
    dataPointWidth: 30,
    data: [{
        type: "column",
        name: "",
        legendText: "",
        showInLegend: true, 
        color : "#3777ec",
        dataPoints:[
            { label: "Cyclizine",fontSize: 1, y: 110000 },
            { label: "Dexamethasone",fontSize: 1, y: 49000 },
            { label: "Diazepam",fontSize: 1, y: 95000 },
            { label: "Docusate sodium",fontSize: 1, y: 85000 },
            { label: "Fluoxetine",fontSize: 1, y: 110000 },
            { label: "Haloperidol",fontSize: 1, y: 85000 },
            { label: "Hyoscine butylbromide",fontSize: 1, y: 65000 },
            { label: "Hyoscine hydrobromide",fontSize: 1, y: 110000 }
        ]
    },
    ]
});
chart.render();

var chart2 = new CanvasJS.Chart("chartContainer2", {
    animationEnabled: true,
    title:{
        text : "Hạn sử dụng còn lại",
        fontFamily: "tahoma",
        verticalAlign: "top",
        horizontalAlign: "left",    
    },  
    axisX:{
        labelFontSize: 10
      },
    axisY: {
        
        lineColor: "black",
        labelFontColor: "black",
        tickColor: "black",
        interval: 1,
        maximum: 11,
        minimum: 1,
        suffix: " tháng"
    },  
    toolTip: {
        shared: true
    },
    legend: {
        cursor:"pointer",
        itemclick: toggleDataSeries
    },
    dataPointWidth: 30,
    data: [{
        type: "column",
        name: "",
        legendText: "",
        showInLegend: true, 
        color : "#3777ec",
        dataPoints:[
            { label: "Cyclizine",fontSize: 1, y: 11 },
            { label: "Dexamethasone",fontSize: 1, y: 4 },
            { label: "Diazepam",fontSize: 1, y: 5 },
            { label: "Docusate sodium",fontSize: 1, y: 3 },
            { label: "Fluoxetine",fontSize: 1, y: 8 },
            { label: "Haloperidol",fontSize: 1, y: 9 },
            { label: "Hyoscine butylbromide",fontSize: 1, y: 6 },
            { label: "Hyoscine hydrobromide",fontSize: 1, y: 10 }
        ]
    },
    ]
});
chart2.render();

var chart3 = new CanvasJS.Chart("chartContainer3", {
    animationEnabled: true,
    title:{
        text : "Thống kê tồn kho",
        fontFamily: "tahoma",
        verticalAlign: "top",
        horizontalAlign: "left",    
    },  
    axisX:{
        labelFontSize: 10
      },
    axisY: {
        
        lineColor: "black",
        labelFontColor: "black",
        tickColor: "black",
        interval: 10000,
        maximum: 110000,
        minimum: 10000,
    },  
    toolTip: {
        shared: true
    },
    legend: {
        cursor:"pointer",
        itemclick: toggleDataSeries
    },
    dataPointWidth: 30,
    data: [{
        type: "column",
        name: "",
        legendText: "",
        showInLegend: true, 
        color : "#3777ec",
        dataPoints:[
            { label: "Cyclizine",fontSize: 1, y: 110000 },
            { label: "Dexamethasone",fontSize: 1, y: 49000 },
            { label: "Diazepam",fontSize: 1, y: 95000 },
            { label: "Docusate sodium",fontSize: 1, y: 85000 },
            { label: "Fluoxetine",fontSize: 1, y: 110000 },
            { label: "Haloperidol",fontSize: 1, y: 85000 },
            { label: "Hyoscine butylbromide",fontSize: 1, y: 65000 },
            { label: "Hyoscine hydrobromide",fontSize: 1, y: 110000 }
        ]
    },
    ]
});
chart3.render();

function toggleDataSeries(e) {
    if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
        e.dataSeries.visible = false;
    }
    else {
        e.dataSeries.visible = true;
    }
    chart.render();
}


    }
</script>
</body>

<!-- Mirrored from thememakker.com/templates/oreo/hospital/html/light/index.php by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Apr 2019 15:00:40 GMT -->
</html>