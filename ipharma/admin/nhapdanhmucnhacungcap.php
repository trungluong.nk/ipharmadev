<?php include './header.php'; ?>
<!-- Main Content -->
<section class="content home">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12">
                <h2>Nhập danh mục nhà cung cấp
                <small>Welcome to iPhama</small>
                </h2>
            </div>            
             <div class="col-lg-7 col-md-7 col-sm-12 text-right">
                <div class="inlineblock text-center m-r-15 m-l-15 hidden-md-down">
                    
                </div>
                <div class="inlineblock text-center m-r-15 m-l-15 hidden-md-down">
                   
                </div>
                <button class="btn btn-white btn-icon btn-round hidden-sm-down float-right m-l-10" type="button">
                    <i class="zmdi zmdi-plus"></i>
                </button>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-home"></i> iPharma</a></li>
                    <li class="breadcrumb-item active">Nhập danh mục nhà cung cấp</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
         <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                       
                        <ul class="header-dropdown">
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        
                        <form>
          <div class="row">
              <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                 <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Tên công ty</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Tên công ty">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Địa chỉ</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Địa chỉ">
                    </div>
                  </div>
                 <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Số điện thoại</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Số điện thoại">
                    </div>
                  </div>
                  
                  
                  <div class="form-group row">
                        <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Loại hình công ty</label>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;margin-top: 0px;">
                         <select class="">
                                    <option value="">Nhà sản xuất</option>
                                    <option value="10">Thương mại</option>
                                    
                        </select>
                        </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Chứng nhận</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Chứng nhận">
                    </div>
                  </div>
                  
                  
              </div>
              <div class="col-12col-sm-12 col-md-12 col-lg-6">
                   <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Người đại diện</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                       <input type="text" class="form-control" id="datepicker" placeholder="Người đại diện">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Chức vụ</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Chức vụ">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Fax</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                       <input type="text" class="form-control" id="datepicker" placeholder="Fax">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Email</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                       <input type="text" class="form-control" id="datepicker" placeholder="Email">
                    </div>
                  </div>
                    <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Mã số thuế</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                       <input type="text" class="form-control" id="datepicker" placeholder="Mã số thuế">
                    </div>
                  </div>
              </div>
          </div>
          <div class="row">
              <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                  <p>Danh mục sản xuất cung cấp</p>
              </div>
              <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                  <div class="col-2 col-sm-2 col-md-2 col-lg-2">
                      
                  </div>
                  <div class="col-10 col-sm-10 col-md-10 col-lg-10">
                      <div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
  <label class="form-check-label" for="inlineCheckbox1">Dược phẩm</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
  <label class="form-check-label" for="inlineCheckbox2">Thuốc đông y</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option3">
  <label class="form-check-label" for="inlineCheckbox3">Thuốc tây</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option3">
  <label class="form-check-label" for="inlineCheckbox3">Thiết bị vật tư y tế</label>
</div>
                  </div>
              </div>
          </div>
        </form>
                        <div class="row clearfix" style="margin-top: 50px;">
                            <div class="col-md-4">
                                <button type="button" class="btn btn-primary" data-whatever="@mdo">Tìm kiếm</button>
                            </div>
                            <div class="col-md-3">
                                
                            </div>
                            <div class="col-md-5">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#centralModalSm" data-whatever="@mdo">Thêm mới</button>
                            </div>

                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                         <ul class="header-dropdown">
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="tab-content m-t-10">
                            <div class="tab-pane table-responsive active" id="All">
                                
                                <table class="table table-hover">
                                    <thead>
                                        <tr>                                       
                                            <th>Tên công ty</th>
                                            <th>Địa chỉ</th>
                                            <th>Số điện thoại</th>
                                            <th>Loại hình công ty</th>
                                            <th>chứng nhận</th>
                                            <th>Người đại diện</th>
                                            <th>Chức vụ</th>
                                            <th>Fax</th>
                                            <th>Email</th>
                                            <th>Mã số thuế</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>                                 
                                            <td>Nhà phân phối 1</td>
                                            <td>Hà Nội</td>
                                            <td>0954355435</td>
                                            <td>Nhà sản xuất</td>
                                            <td>Chứng nhận</td>
                                            <td>Nguyễn Văn A</td>
                                            <td>Nhân viên</td>
                                            <td>Fax 1</td>
                                            <td>nguyenvana@gmail.com</td>
                                            <td>1243454</td>
                                            
                                            
                                        </tr>
                                        <tr>                                 
                                            <td>Nhà phân phối 1</td>
                                            <td>Hà Nội</td>
                                            <td>0954355435</td>
                                            <td>Nhà sản xuất</td>
                                            <td>Chứng nhận</td>
                                            <td>Nguyễn Văn A</td>
                                            <td>Nhân viên</td>
                                            <td>Fax 1</td>
                                            <td>nguyenvana@gmail.com</td>
                                            <td>1243454</td>
                                            
                                            
                                        </tr>
                                        <tr>                                 
                                            <td>Nhà phân phối 1</td>
                                            <td>Hà Nội</td>
                                            <td>0954355435</td>
                                            <td>Nhà sản xuất</td>
                                            <td>Chứng nhận</td>
                                            <td>Nguyễn Văn A</td>
                                            <td>Nhân viên</td>
                                            <td>Fax 1</td>
                                            <td>nguyenvana@gmail.com</td>
                                            <td>1243454</td>
                                            
                                            
                                        </tr>
                                        <tr>                                 
                                            <td>Nhà phân phối 1</td>
                                            <td>Hà Nội</td>
                                            <td>0954355435</td>
                                            <td>Nhà sản xuất</td>
                                            <td>Chứng nhận</td>
                                            <td>Nguyễn Văn A</td>
                                            <td>Nhân viên</td>
                                            <td>Fax 1</td>
                                            <td>nguyenvana@gmail.com</td>
                                            <td>1243454</td>
                                            
                                            
                                        </tr>
                                        <tr>                                 
                                            <td>Nhà phân phối 1</td>
                                            <td>Hà Nội</td>
                                            <td>0954355435</td>
                                            <td>Nhà sản xuất</td>
                                            <td>Chứng nhận</td>
                                            <td>Nguyễn Văn A</td>
                                            <td>Nhân viên</td>
                                            <td>Fax 1</td>
                                            <td>nguyenvana@gmail.com</td>
                                            <td>1243454</td>
                                            
                                            
                                        </tr>
                                        <tr>                                 
                                            <td>Nhà phân phối 1</td>
                                            <td>Hà Nội</td>
                                            <td>0954355435</td>
                                            <td>Nhà sản xuất</td>
                                            <td>Chứng nhận</td>
                                            <td>Nguyễn Văn A</td>
                                            <td>Nhân viên</td>
                                            <td>Fax 1</td>
                                            <td>nguyenvana@gmail.com</td>
                                            <td>1243454</td>
                                            
                                            
                                        </tr>
                                        
                                         
                                        
                                    </tbody>
                                </table>                            
                            </div>
                           
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
                  
    </div>


</section>

<!-- Central Modal Small -->
<div class="modal fade" id="centralModalSm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Change class .modal-sm to change the size of the modal -->
  <div class="modal-dialog modal-lg" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Thêm mới</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form>
          <div class="row">
              <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                 <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Tên công ty</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Tên công ty" style="width: 91%;margin-left: 18px;">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Địa chỉ</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Địa chỉ" style="width: 91%;margin-left: 18px;">
                    </div>
                  </div>
                 <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Số điện thoại</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Số điện thoại" style="width: 91%;margin-left: 18px;">
                    </div>
                  </div>
                  
                  
                  <div class="form-group row">
                        <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Loại hình công ty</label>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;margin-top: -10px;">
                         <select class="form-control show-tick">
                                    <option value="">Nhà sản xuất</option>
                                    <option value="10">Thương mại</option>
                                    
                        </select>
                        </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Chứng nhận</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Chứng nhận" style="width: 91%;margin-left: 18px;">
                    </div>
                  </div>
                  
                  
              </div>
              <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                   <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Người đại diện</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                       <input type="text" class="form-control" id="datepicker" placeholder="Người đại diện">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Chức vụ</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Chức vụ">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Fax</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                       <input type="text" class="form-control" id="datepicker" placeholder="Fax">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Email</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                       <input type="text" class="form-control" id="datepicker" placeholder="Email">
                    </div>
                  </div>
                    <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Mã số thuế</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                       <input type="text" class="form-control" id="datepicker" placeholder="Mã số thuế">
                    </div>
                  </div>
              </div>
          </div>
          <div class="row">
              <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                  <p>Danh mục sản xuất cung cấp</p>
              </div>
              <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                  <div class="col-2 col-sm-2 col-md-2 col-lg-2">
                      
                  </div>
                  <div class="col-10 col-sm-10 col-md-10 col-lg-10">
                      <div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
  <label class="form-check-label" for="inlineCheckbox1">Dược phẩm</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
  <label class="form-check-label" for="inlineCheckbox2">Thuốc đông y</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option3">
  <label class="form-check-label" for="inlineCheckbox3">Thuốc tây</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option3">
  <label class="form-check-label" for="inlineCheckbox3">Thiết bị vật tư y tế</label>
</div>
                  </div>
              </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        
        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal">Thêm mới</button>
      </div>
    </div>
  </div>
</div>
<!-- Central Modal Small -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Thông báo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Thêm mới thành công
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>

<!-- Jquery Core Js --> 


<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Bootstrap JS and jQuery v3.2.1 -->
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- slimscroll, waves Scripts Plugin Js -->  

<script src="plugins/dropzone/dropzone.js"></script> <!-- Dropzone Plugin Js -->
<script src="plugins/momentjs/moment.js"></script> <!-- Moment Plugin Js -->
<script src="plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

<script src="assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js -->
<script>
    $(function () {
    //Datetimepicker plugin
    $('.datetimepicker').bootstrapMaterialDatePicker({
        format: 'dddd DD MMMM YYYY - HH:mm',
        clearButton: true,
        weekStart: 1
    });
});
</script>
</body>

<!-- Mirrored from thememakker.com/templates/oreo/hospital/html/light/index.php by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Apr 2019 15:00:40 GMT -->
</html>