<?php include './header.php'; ?>
<!-- Main Content -->
<section class="content home">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12">
                <h2>Bảng xếp hạng
                    <small>Welcome to iPhama</small>
                </h2>
            </div>            
            <div class="col-lg-7 col-md-7 col-sm-12 text-right">
                <div class="inlineblock text-center m-r-15 m-l-15 hidden-md-down">

                </div>
                <div class="inlineblock text-center m-r-15 m-l-15 hidden-md-down">

                </div>
                <button class="btn btn-white btn-icon btn-round hidden-sm-down float-right m-l-10" type="button">
                    <i class="zmdi zmdi-plus"></i>
                </button>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-home"></i> iPharma</a></li>
                    <li class="breadcrumb-item active">Bảng xếp hạng</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">

                        <ul class="header-dropdown">
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <form>
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-6">

                                    <div class="form-group row">
                                        <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Vị trí nhà thuốc</label>
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;">
                                            <input type="text" class="form-control" id="inputPassword" placeholder="Vị trí nhà thuốc">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 10px;">Tên nhà thuốc</label>
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;">
                                            <input type="text" class="form-control" id="inputPassword" placeholder="Tên nhà thuốc">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 10px;">Lượt đánh giá</label>
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;">
                                            <input type="text" class="form-control" id="inputPassword" placeholder="Lượt đánh giá">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 10px;">Lượt comment</label>
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;">
                                            <input type="text" class="form-control" id="inputPassword" placeholder="Lượt comment">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-6">

                                    <div class="form-group row">
                                        <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Mã khách hàng</label>
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;">
                                            <input type="text" class="form-control" id="inputPassword" placeholder="Mã khách hàng">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Lượt đơn hàng thành công</label>
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;">
                                            <input type="text" class="form-control" id="datepicker" placeholder="Lượt đơn hàng thành công">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Tổng số đơn hàng</label>
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;">
                                            <input type="text" class="form-control" id="datepicker" placeholder="Tổng số đơn hàng">
                                        </div>
                                    </div>
                                    <div class="form-group row" style="margin-top: 25px;">
                                        <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Rating</label>
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;margin-top: -5px;">
                                            <select class="">
                                                <option value="">1 sao</option>
                                                <option value="10">2 sao</option>
                                                <option value="20">3 sao</option>
                                                <option value="20">4 sao</option>
                                                <option value="20">5 sao</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="row clearfix">
                            <div class="col-md-3">

                            </div>
                            <div class="col-md-3">
                                <button type="button" class="btn btn-primary"  data-whatever="@mdo">Tìm kiếm</button>

                            </div>
                            <div class="col-md-3">

                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <ul class="header-dropdown">
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="tab-content m-t-10">
                            <div class="tab-pane table-responsive active" id="All">

                                <table class="table table-hover">
                                    <thead>
                                        <tr>                                       
                                            <th>Vị trí nhà thuốc</th>
                                            <th>Tên nhà thuốc</th>
                                            <th>Lượt đánh giá</th>
                                            <th>Lượt comment</th>
                                            <th>Lượt đơn hàng thành công</th>
                                            <th>Tổng số đơn hàng</th>
                                            <th>Rating</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>                                 
                                            <td>195</td>
                                            <td>iPhrama</td>
                                            <td>15000</td>
                                            <td>20000</td>
                                            <td>280</td>
                                            <td>300</td>
                                            <td>
                                                <i class="fas fa-star" style="color: #ffeb00;"></i>
                                                <i class="fas fa-star" style="color: #ffeb00;"></i>
                                                <i class="fas fa-star" style="color: #ffeb00;"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </td>
                                        </tr>
                                        <tr>                                 
                                            <td>194</td>
                                            <td>Nhà thuốc Nam Cường</td>
                                            <td>15000</td>
                                            <td>20000</td>
                                            <td>280</td>
                                            <td>300</td>
                                            <td>
                                                <i class="fas fa-star" style="color: #ffeb00;"></i>
                                                <i class="fas fa-star" style="color: #ffeb00;"></i>
                                                <i class="fas fa-star" style="color: #ffeb00;"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </td>
                                        </tr>
                                        <tr>                                 
                                            <td>193</td>
                                            <td>Nhà thuốc </td>
                                            <td>15000</td>
                                            <td>20000</td>
                                            <td>280</td>
                                            <td>300</td>
                                            <td>
                                                <i class="fas fa-star" style="color: #ffeb00;"></i>
                                                <i class="fas fa-star" style="color: #ffeb00;"></i>
                                                <i class="fas fa-star" style="color: #ffeb00;"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </td>
                                        </tr>
                                        <tr>                                 
                                            <td>192</td>
                                            <td>Nhà thuốc </td>
                                            <td>15000</td>
                                            <td>20000</td>
                                            <td>280</td>
                                            <td>300</td>
                                            <td>
                                                <i class="fas fa-star" style="color: #ffeb00;"></i>
                                                <i class="fas fa-star" style="color: #ffeb00;"></i>
                                                <i class="fas fa-star" style="color: #ffeb00;"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </td>
                                        </tr>
                                        <tr>                                 
                                            <td>191</td>
                                            <td>Nhà thuốc </td>
                                            <td>15000</td>
                                            <td>20000</td>
                                            <td>280</td>
                                            <td>300</td>
                                            <td>
                                                <i class="fas fa-star" style="color: #ffeb00;"></i>
                                                <i class="fas fa-star" style="color: #ffeb00;"></i>
                                                <i class="fas fa-star" style="color: #ffeb00;"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </td>
                                        </tr>
                                        <tr>                                 
                                            <td>190</td>
                                            <td>Nhà thuốc </td>
                                            <td>15000</td>
                                            <td>20000</td>
                                            <td>280</td>
                                            <td>300</td>
                                            <td>
                                                <i class="fas fa-star" style="color: #ffeb00;"></i>
                                                <i class="fas fa-star" style="color: #ffeb00;"></i>
                                                <i class="fas fa-star" style="color: #ffeb00;"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </td>
                                        </tr>


                                    </tbody>
                                </table>                            
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


</section>

<!-- Central Modal Small -->
<div class="modal fade" id="centralModalSm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">

    <!-- Change class .modal-sm to change the size of the modal -->
    <div class="modal-dialog modal-lg" role="document">


        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title w-100" id="myModalLabel">Thêm mới</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                            <div class="form-group row">
                                <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 15px;padding-right: 0px;">Loại đối tác</label>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                                    <select class="form-control show-tick" style="width:90%;">
                                        <option value="">Đối tác 1</option>
                                        <option value="10">Đối tác 2</option>
                                        <option value="20">Đối tác 3</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Liên hệ ID</label>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                                    <input type="text" class="form-control" id="inputPassword" placeholder="Liên hệ ID" style="width: 88%;margin-left: 18px;">
                                </div>
                            </div>
                            <div class="form-group row" style="width: 99%;">
                                <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Chi tiết</label>

                                <textarea class="form-control col-12 col-sm-12 col-md-12 col-lg-9" id="exampleFormControlTextarea1" rows="2" style="width: 80%;margin-left: 17px;border: 1px solid #eee;border-radius: 100px"></textarea>

                            </div>


                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-6">

                            <div class="form-group row">
                                <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 15px;padding-right: 0px;">Nhà thuốc ID</label>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                                    <input type="text" class="form-control" id="inputPassword" placeholder="Nhà thuốc ID" style="width: 88%;margin-left: 18px;">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Đơn vị liên hệ</label>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                                    <input type="text" class="form-control" id="datepicker" placeholder="Tên đơn vị liên hệ" style="width: 88%;margin-left: 18px;">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal">Thêm mới</button>
            </div>
        </div>
    </div>
</div>
<!-- Central Modal Small -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Thông báo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Thêm mới thành công
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>

<!-- Jquery Core Js --> 


<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Bootstrap JS and jQuery v3.2.1 -->
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- slimscroll, waves Scripts Plugin Js -->  

<script src="plugins/dropzone/dropzone.js"></script> <!-- Dropzone Plugin Js -->
<script src="plugins/momentjs/moment.js"></script> <!-- Moment Plugin Js -->
<script src="plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

<script src="assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js -->
<script>
    $(function () {
        //Datetimepicker plugin
        $('.datetimepicker').bootstrapMaterialDatePicker({
            format: 'dddd DD MMMM YYYY - HH:mm',
            clearButton: true,
            weekStart: 1
        });
    });
</script>
</body>

<!-- Mirrored from thememakker.com/templates/oreo/hospital/html/light/index.php by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Apr 2019 15:00:40 GMT -->
</html>