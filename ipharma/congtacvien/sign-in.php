﻿<!doctype html>
<html class="no-js " lang="en">

    <!-- Mirrored from thememakker.com/templates/oreo/hospital/html/light/sign-in.php by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Apr 2019 15:00:53 GMT -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

        <title>:: ipharma :: Sign In</title>
        <!-- Favicon-->
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <!-- Custom Css -->
        <link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/main.css">
        <link rel="stylesheet" href="assets/css/authentication.css">
        <link rel="stylesheet" href="assets/css/color_skins.css">
        <style type="text/css">
            .btn-facebook {
                background-color: #3B5998;
                color: white;
            }
            .btn-google {
                background-color: #dd4b39;
                color: white;
            }
            .btn:hover {
                opacity: 0.5;
                color: #fff;
            }
        </style>
    </head>

    <body class="theme-cyan authentication sidebar-collapse">
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg fixed-top navbar-transparent">
            <div class="container">        
                <div class="navbar-translate n_logo">
                    <a class="navbar-brand" href="javascript:void(0);" title="" target="_blank">iPharma</a>
                    <button class="navbar-toggler" type="button">
                        <span class="navbar-toggler-bar bar1"></span>
                        <span class="navbar-toggler-bar bar2"></span>
                        <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <div class="navbar-collapse">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="index.php">Trang chủ</a>
                        </li>




                        <li class="nav-item">
                            <a class="nav-link btn btn-white btn-round" href="sign-up.php">ĐĂNG KÍ</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- End Navbar -->
        <div class="page-header">
            <div class="page-header-image" style="background-image:url(images/login.jpg)"></div>
            <div class="container">
                <div class="col-md-12 content-center">
                    <div class="card-plain">
                        <form class="form" method="" action="#">
                            <div class="header">
                                <div class="logo-container">
                                    <img src="images/logosn.png" alt="">
                                </div>
                                <h5>ĐĂNG NHẬP</h5>
                            </div>
                            <div class="content">

                                <a href="index.php" class="btn btn-facebook btn-round btn-lg btn-block ">Đăng nhập bằng Facebook</a>
                                <a href="index.php" class="btn btn-google btn-round btn-lg btn-block ">Đăng nhập bằng Google</a>
                                <div class="text-hoac">Hoặc</div>
                                <div class="input-group input-lg">
                                    <input type="text" class="form-control" placeholder="Số điện thoại hoặc email">
                                    <span class="input-group-addon">
                                        <i class="zmdi zmdi-account-circle"></i>
                                    </span>
                                </div>
                                <div class="input-group input-lg">
                                    <input type="password" placeholder="Mật khẩu" class="form-control" />
                                    <span class="input-group-addon">
                                        <i class="zmdi zmdi-lock"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="footer text-center">
                                <a href="index.php" class="btn btn-primary btn-round btn-lg btn-block ">ĐĂNG NHẬP</a>
                                <h5><a href="forgot-password.php" class="link">Quên mật khẩu?</a></h5>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>

        <!-- Jquery Core Js -->
        <script src="assets/bundles/libscripts.bundle.js"></script>
        <script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->

        <script>
            $(".navbar-toggler").on('click', function () {
                $("html").toggleClass("nav-open");
            });
        //=============================================================================
            $('.form-control').on("focus", function () {
                $(this).parent('.input-group').addClass("input-group-focus");
            }).on("blur", function () {
                $(this).parent(".input-group").removeClass("input-group-focus");
            });
        </script>
    </body>

    <!-- Mirrored from thememakker.com/templates/oreo/hospital/html/light/sign-in.php by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Apr 2019 15:00:53 GMT -->
</html>