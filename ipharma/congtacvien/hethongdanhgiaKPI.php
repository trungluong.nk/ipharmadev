<?php include './header.php'; ?>
<!-- Main Content -->
<section class="content home">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12">
                <h2>Cấu hình > Hệ thống đánh giá KPI
                <small>Welcome to iPhama</small>
                </h2>
            </div>            
             <div class="col-lg-7 col-md-7 col-sm-12 text-right">
                <div class="inlineblock text-center m-r-15 m-l-15 hidden-md-down">
                    
                </div>
                <div class="inlineblock text-center m-r-15 m-l-15 hidden-md-down">
                   
                </div>
                <button class="btn btn-white btn-icon btn-round hidden-sm-down float-right m-l-10" type="button">
                    <i class="zmdi zmdi-plus"></i>
                </button>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-home"></i> iPharma</a></li>
                    <li class="breadcrumb-item active">Hệ thống đánh giá KPI</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
         <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                       
                        <ul class="header-dropdown">
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <form>
          <div class="row">
              <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                  
                  <div class="form-group row">
                        <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-4 col-form-label" style="font-size: 15px;padding-left: 15px;padding-right: 0px;">Đánh giá KPI theo</label>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-8" style="padding-left: 0px;padding-right: -1px;margin-top: -10px;">
                         <select class="form-control show-tick" style="width: 97%;">
                                    <option value="">Tuần</option>
                                    <option value="10">Tháng</option>
                                    <option value="20">Năm</option>
                        </select>
                        </div>
                  </div>
                   
                  
                  
              </div>
              <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                   
                  
                  
                 
              </div>
          </div>
        </form>
                        <div class="row clearfix">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-8">
                                <div id="chartContainer1" style="height: 300px; width: 100%;"></div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                                 <div id="chartContainer2" style="height: 300px; width: 100%;"></div>
                            </div>
                           
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                         <ul class="header-dropdown">
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-8">
                                <div id="chartContainer3" style="height: 300px; width: 100%;"></div>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                                 <div id="chartContainer4" style="height: 300px; width: 100%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                         <ul class="header-dropdown">
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="tab-content m-t-10">
                            <div class="tab-pane table-responsive active" id="All">
                                
                                <table class="table table-hover">
                                    <thead>
                                        <tr>                                       
                                            <th>Tuần 1</th>
                                            <th>Tuần 2</th>
                                            <th>Tuần 3</th>
                                            <th>Tuần 4</th>
                                            <th>Tuần 5</th>
                                            <th>Tuần 6</th>
                                            <th>Tuần 7</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>                                 
                                            <td>5435</td>
                                            <td>53543</td>
                                            <td>53453</td>
                                            <td>5435345</td>
                                            <td>5435345</td>
                                            <td>435345</td>
                                            <td>435454</td>
                                            
                                        </tr>
                                         <tr>                                 
                                            <td>34535</td>
                                            <td>5343</td>
                                            <td>54335</td>
                                            <td>5435345</td>
                                            <td>4535345</td>
                                            <td>4335345</td>
                                            <td>43535345</td>
                                            
                                        </tr>
                                        <tr>                                 
                                            <td>5435353</td>
                                            <td>5435343</td>
                                            <td>54335345</td>
                                            <td>53453453</td>
                                            <td>534535</td>
                                            <td>545345</td>
                                            <td>545345</td>
                                            
                                        </tr>
                                        <tr>                                 
                                            <td>5435345</td>
                                            <td>545353</td>
                                            <td>545353</td>
                                            <td>545353</td>
                                            <td>5345353</td>
                                            <td>5345435</td>
                                            <td>5345355</td>
                                            
                                        </tr>
                                        
                                    </tbody>
                                </table>                            
                            </div>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
                  
    </div>


</section>

<!-- Central Modal Small -->
<div class="modal fade" id="centralModalSm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Change class .modal-sm to change the size of the modal -->
  <div class="modal-dialog modal-lg" role="document">


    
  </div>
</div>
<!-- Central Modal Small -->



<!-- Jquery Core Js --> 


<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Bootstrap JS and jQuery v3.2.1 -->
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- slimscroll, waves Scripts Plugin Js -->  

<script src="plugins/dropzone/dropzone.js"></script> <!-- Dropzone Plugin Js -->
<script src="plugins/momentjs/moment.js"></script> <!-- Moment Plugin Js -->
<script src="plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<script src="assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js -->
<script>
    $(function () {
    //Datetimepicker plugin
    $('.datetimepicker').bootstrapMaterialDatePicker({
        format: 'dddd DD MMMM YYYY - HH:mm',
        clearButton: true,
        weekStart: 1
    });
});
</script>
<script type="text/javascript">
    window.onload = function () {
        var chart1 = new CanvasJS.Chart("chartContainer1", {
    animationEnabled: true,
    title:{
        text : "",
        fontFamily: "tahoma",
        verticalAlign: "top",
        horizontalAlign: "left",    
    },  
    axisX:{
        labelFontSize: 10
      },
    axisY: {
        
        lineColor: "black",
        labelFontColor: "black",
        tickColor: "black",
        interval: 20000,
        maximum: 110000,
        minimum: 10000,
    },  
    toolTip: {
        shared: true
    },
    legend: {
        cursor:"pointer",
        itemclick: toggleDataSeries
    },
    dataPointWidth: 60,
    data: [{
        type: "column",
        name: "",
        legendText: "",
        showInLegend: true, 
        color : "#3777ec",
        dataPoints:[
            { label: "Tuần 1",fontSize: 1, y: 110000 },
            { label: "Tuần 2",fontSize: 1, y: 49000 },
            { label: "Tuần 3",fontSize: 1, y: 95000 },
            { label: "Tuần 4",fontSize: 1, y: 85000 },
            { label: "Tuần 5",fontSize: 1, y: 110000 }
            
        ]
    },
    ]
});
chart1.render();

var chart2 = new CanvasJS.Chart("chartContainer2", {
    theme: "light2",
    animationEnabled: true,
    title: {
        text: ""
    },
    subtitles: [{
        text: "",
        fontSize: 16
    }],
    data: [{
        type: "pie",
        indexLabelFontSize: 18,
        radius: 80,
        indexLabel: "{label} - {y}",
        yValueFormatString: "###0.0\"%\"",
        click: explodePie,
        dataPoints: [
            { y: 3, label: "Internet Explorer" },
            { y: 7, label: "Mobie"},
            { y: 22, label: "safari" },
            { y: 9,label : "Opera" },
            { y: 23, label: "Fire fox" },
            { y: 29, label: "Chrome" },
        ]
    }]
});
chart2.render();

var chart3 = new CanvasJS.Chart("chartContainer3", {
    animationEnabled: true,
    title:{
        text: "Mốc hoàn thành KPI",
        fontFamily: "tahoma",
        verticalAlign: "top",
        horizontalAlign: "left",  
    },
    axisX: {

    },
    axisY: {
        title: "",
         interval: 1,
        maximum: 5,
        minimum: 0,
        prefix: "level "
    },
    legend: {
        cursor:"pointer",
        itemclick : toggleDataSeries
    },
    toolTip: {
        shared: true,
        content: toolTipFormatter
    },
    dataPointWidth: 30,
    data: [{
        type: "bar",
        showInLegend: true,
        name: "Mức độ hoàn thành",
        color: "#3777ec",
        dataPoints: [
            { y: 5, label: "job 5" },
            { y: 2, label: "job 4" },
            { y: 3, label: "job 3" },
            { y: 4, label: "job 2" }
        ]
    }]

});
chart3.render();

var chart4 = new CanvasJS.Chart("chartContainer4", {
    theme: "light2",
    animationEnabled: true,
    title: {
        text: ""
    },
    subtitles: [{
        text: "",
        fontSize: 16
    }],
    data: [{
        type: "pie",
        indexLabelFontSize: 18,
        radius: 80,
        indexLabel: "{label} - {y}",
        yValueFormatString: "###0.0\"%\"",
        click: explodePie,
        dataPoints: [
            { y: 3, label: "Internet Explorer" },
            { y: 7, label: "Mobie"},
            { y: 22, label: "safari" },
            { y: 9,label : "Opera" },
            { y: 23, label: "Fire fox" },
            { y: 29, label: "Chrome" },
        ]
    }]
});
chart4.render();

function toolTipFormatter(e) {
    var str = "";
    var total = 0 ;
    var str3;
    var str2 ;
    for (var i = 0; i < e.entries.length; i++){
        var str1 = "<span style= \"color:"+e.entries[i].dataSeries.color + "\">" + e.entries[i].dataSeries.name + "</span>: <strong>"+  e.entries[i].dataPoint.y + "</strong> <br/>" ;
        total = e.entries[i].dataPoint.y + total;
        str = str.concat(str1);
    }
    str2 = "<strong>" + e.entries[0].dataPoint.label + "</strong> <br/>";
    str3 = "<span style = \"color:Tomato\">Total: </span><strong>" + total + "</strong><br/>";
    return (str2.concat(str)).concat(str3);
}

function toggleDataSeries(e) {
    if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
        e.dataSeries.visible = false;
    }
    else {
        e.dataSeries.visible = true;
    }
    chart.render();
}


function explodePie(e) {
    for(var i = 0; i < e.dataSeries.dataPoints.length; i++) {
        if(i !== e.dataPointIndex)
            e.dataSeries.dataPoints[i].exploded = false;
    }
}

function explodePie(e) {
    for(var i = 0; i < e.dataSeries.dataPoints.length; i++) {
        if(i !== e.dataPointIndex)
            e.dataSeries.dataPoints[i].exploded = false;
    }
}

function toggleDataSeries(e) {
    if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
        e.dataSeries.visible = false;
    }
    else {
        e.dataSeries.visible = true;
    }
    chart.render();
}
    }
</script>
</body>

<!-- Mirrored from thememakker.com/templates/oreo/hospital/html/light/index.php by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Apr 2019 15:00:40 GMT -->
</html>