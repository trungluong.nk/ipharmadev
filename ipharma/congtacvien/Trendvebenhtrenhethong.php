<?php include './header.php'; ?>
<!-- Main Content -->
<section class="content home">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12">
                <h2>Thống kê > Trend về bệnh trên hệ thống
                <small>Welcome to iPhama</small>
                </h2>
            </div>            
            <div class="col-lg-7 col-md-7 col-sm-12 text-right">
                <div class="inlineblock text-center m-r-15 m-l-15 hidden-md-down">
                    
                </div>
                <div class="inlineblock text-center m-r-15 m-l-15 hidden-md-down">
                   
                </div>
                <button class="btn btn-white btn-icon btn-round hidden-sm-down float-right m-l-10" type="button">
                    <i class="zmdi zmdi-plus"></i>
                </button>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-home"></i> iPharma</a></li>
                    <li class="breadcrumb-item active">Trend về bệnh trên hệ thống</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
         <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                       
                        <ul class="header-dropdown">
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                       <form>
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                                      
                                     <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Search thời gian</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                       <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="zmdi zmdi-calendar"></i>
                                    </span>
                                    <input type="text" class="datetimepicker form-control" placeholder="Nhập thời gian bắt đầu">
                                </div>   
                    </div>
                  </div>
                                      <div class="form-group row">
                                        <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Loại thuốc</label>
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                                          <input type="text" class="form-control" id="inputPassword" placeholder="Loại thuốc">
                                        </div>
                                      </div>     
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                                     
                                      <div class="form-group row">
                        <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 15px;padding-right: 0px;">Hiển thị dữ liệu theo</label>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;margin-top: -4px;">
                         <select class="">
                                    <option value="">Tuần</option>
                                    <option value="10">Tháng</option>
                                    <option value="20">Quý</option>
                                    <option value="20">Năm</option>
                        </select>
                        </div>
                  </div>
                   <div class="form-group row" style="margin-top: 20px;">
                                        <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Loại bệnh</label>
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                                          <input type="text" class="form-control" id="inputPassword" placeholder="Loại bệnh">
                                        </div>
                                      </div>  
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4 col-sm-4 col-md-4 col-lg-4">
                                    
                                </div>
                                <div class="col-4 col-sm-4 col-md-4 col-lg-4">
                                     <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#centralModalSm" data-whatever="@mdo">Tìm kiếm</button>
                                </div>
                                <div class="col-4 col-sm-4 col-md-4 col-lg-4">
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#centralModalSm" data-whatever="@mdo">Xuất báo cáo</button>
                                </div>
                            </div>
                        </form>       
                        
                    </div>
                    
                </div>
            </div>
            
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                       
                        <ul class="header-dropdown">
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="row">
                            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                                 <div id="chartContainer2" style="height: 300px; width: 100%;"></div>
                            </div>
                             <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                                 <div id="chartContainer3" style="height: 300px; width: 100%;"></div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
                  
    </div>


</section>

<!-- Central Modal Small -->

<!-- Central Modal Small -->



<!-- Jquery Core Js --> 


<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Bootstrap JS and jQuery v3.2.1 -->
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- slimscroll, waves Scripts Plugin Js -->  

<script src="plugins/dropzone/dropzone.js"></script> <!-- Dropzone Plugin Js -->
<script src="plugins/momentjs/moment.js"></script> <!-- Moment Plugin Js -->
<script src="plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<script src="assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js -->
<script>
    $(function () {
    //Datetimepicker plugin
    $('.datetimepicker').bootstrapMaterialDatePicker({
        format: 'dddd DD MMMM YYYY - HH:mm',
        clearButton: true,
        weekStart: 1
    });
});
</script>
<script type="text/javascript">
    window.onload = function () {
        
var chart2 = new CanvasJS.Chart("chartContainer2", {
    theme: "light2",
    animationEnabled: true,
    title: {
        text: "Biểu đồ loại thuốc đang được sử dụng nhiều nhất",
        fontFamily: "tahoma",
        verticalAlign: "top",
        horizontalAlign: "left",    
         fontSize : 15  
    },
    subtitles: [{
        text: "",
        fontSize: 16
    }],
    data: [{
        type: "pie",
        indexLabelFontSize: 18,
        radius: 80,
        indexLabel: "{label} - {y}",
        yValueFormatString: "###0.0\"%\"",
        click: explodePie,
        dataPoints: [
            { y: 3, label: "Internet Explorer" },
            { y: 7, label: "Mobie"},
            { y: 22, label: "safari" },
            { y: 9,label : "Opera" },
            { y: 23, label: "Fire fox" },
            { y: 29, label: "Chrome" },
        ]
    }]
});
chart2.render();


function explodePie(e) {
    for(var i = 0; i < e.dataSeries.dataPoints.length; i++) {
        if(i !== e.dataPointIndex)
            e.dataSeries.dataPoints[i].exploded = false;
    }
}

var chart3 = new CanvasJS.Chart("chartContainer3", {
    animationEnabled: true,
    title:{
        text: "Biểu đồ các bệnh tương ứng thuốc",
        fontFamily: "tahoma",
        verticalAlign: "top",
        horizontalAlign: "left",    
         fontSize : 15,
         fontWeight : "bold" 
    },
    axisX: {
        title: "ngày",
        
        align: "right"
    },
    axisY:{
        interval: 10,
        maximum: 70,
        minimum: 0,
    },
    toolTip: {
        shared: true,
        content: toolTipContent
    },
    legend: {
        horizontalAlign: "right",
        verticalAlign: "top"
      },
    data: [{
        type: "stackedColumn",
        showInLegend: false,
        color: "#ff8721",
        name: "Q1",
        dataPoints: [
            { y: 6.75, label: "1" },
            { y: 8.57, label: "2" },
            { y: 10.64, label: "3" },
            { y: 13.97, label: "4" },
            { y: 15.42, label: "5" },
            { y: 17.26,  label: "6"},
            { y: 20.26, label: "7" }
        ]
        },
        {        
            type: "stackedColumn",
            showInLegend: false,
            name: "Q2",
            color: "#407edc",
            dataPoints: [
                { y: 6.82, label:"1" },
                { y: 9.02, label:"2" },
                { y: 11.80, label:"3" },
                { y: 14.11, label:"4" },
                { y: 15.96, label:"5" },
                { y: 17.73, label:"6" },
                { y: 21.5, label:"7" }
            ]
        },
        {        
            type: "stackedColumn",
            showInLegend: false,
            name: "Q3",
            color: "#cc1b1b",
            dataPoints: [
                { y: 7.28, label: "1" },
                { y: 9.72, label: "2"  },
                { y: 13.30, label: "3"  },
                { y: 14.9, label: "4"  },
                { y: 18.10, label: "5" },
                { y: 18.68, label: "6" },
                { y: 22.45, label : "7" }
            ]
        }]
});
chart3.render();

function toolTipContent(e) {
    var str = "";
    var total = 0;
    var str2, str3;
    for (var i = 0; i < e.entries.length; i++){
        var  str1 = "<span style= \"color:"+e.entries[i].dataSeries.color + "\"> "+e.entries[i].dataSeries.name+"</span>: $<strong>"+e.entries[i].dataPoint.y+"</strong>bn<br/>";
        total = e.entries[i].dataPoint.y + total;
        str = str.concat(str1);
    }
    str2 = "<span style = \"color:DodgerBlue;\"><strong>"+(e.entries[0].dataPoint.x).getFullYear()+"</strong></span><br/>";
    total = Math.round(total * 100) / 100;
    str3 = "<span style = \"color:Tomato\">Total:</span><strong> $"+total+"</strong>bn<br/>";
    return (str2.concat(str)).concat(str3);
}

function explodePie(e) {
    for(var i = 0; i < e.dataSeries.dataPoints.length; i++) {
        if(i !== e.dataPointIndex)
            e.dataSeries.dataPoints[i].exploded = false;
    }
}
    }
</script>
</body>

<!-- Mirrored from thememakker.com/templates/oreo/hospital/html/light/index.php by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Apr 2019 15:00:40 GMT -->
</html>