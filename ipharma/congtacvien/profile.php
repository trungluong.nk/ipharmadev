<?php include './header.php'; ?>
<!-- Main Content -->
<section class="content profile-page">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Profile
                <small>Welcome to iPharma</small>
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">                
                <button class="btn btn-white btn-icon btn-round hidden-sm-down float-right m-l-10" type="button">
                    <i class="zmdi zmdi-edit"></i>
                </button>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-home"></i> iPharma</a></li>
                    <li class="breadcrumb-item active">Profile</li>
                </ul>                
            </div>
        </div>
    </div>    
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-4 col-md-12">
                <div class="card profile-header">
                    <div class="body text-center">
                        <div class="profile-image"> <img src="images/profile_av.jpg" alt=""> </div>
                        <div>
                            <h4 class="m-b-0"><strong>Cộng tác viên iPharma</strong> </h4>
                            <p>Tòa nhà Sông Đà, Phạm Hùng, Phạm Hùng, Mỹ Đình, Nam Từ Liêm, Hà Nội</p>
                        </div>
                        <div>
                            <button class="btn btn-primary btn-round">Follow</button>
                            <button class="btn btn-primary btn-round btn-simple">Message</button>
                        </div>
                        <p class="social-icon m-t-5 m-b-0">
                            <a title="Twitter" href="javascript:void(0);"><i class="zmdi zmdi-twitter"></i></a>
                            <a title="Facebook" href="javascript:void(0);"><i class="zmdi zmdi-facebook"></i></a>
                            <a title="Google-plus" href="javascript:void(0);"><i class="zmdi zmdi-twitter"></i></a>
                            <a title="Behance" href="javascript:void(0);"><i class="zmdi zmdi-behance"></i></a>
                            <a title="Instagram" href="javascript:void(0);"><i class="zmdi zmdi-instagram "></i></a>
                        </p>
                    </div>                    
                </div>                               
               
                              
            </div>
            <div class="col-lg-8 col-md-12">
                <div class="card">
                    <ul class="nav nav-tabs">
                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#about">Cộng tác viên iPharma</a></li>
                                              
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane body active" id="about">
                           
                            <div class="row">
                                <div class="col-2 col-sm-2 col-md-2 col-lg-2">
                                    <p>ĐT</p>
                                </div>
                                <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                                    <p>0245588699 | 0960823699</p>
                                </div>
                                <div class="col-4 col-sm-4 col-md-4 col-lg-4">
                                    <a href="#" title="">Chỉnh sửa</a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-2 col-sm-2 col-md-2 col-lg-2">
                                    <p>Email</p>
                                </div>
                                <div class="col-6 col-sm-6 col-md-6 col-lg-6">ipharma@gmail.com</div>
                            </div>
                           <div class="row">
                               <div class="col-2 col-sm-2 col-md-2 col-lg-2">
                                    <p>Địa chỉ</p>
                               </div>
                               <div class="col-8 col-sm-8 col-md-8 col-lg-8">
                                   <p>Tòa nhà Sông Đà, Phạm Hùng, Phạm Hùng, Mỹ Đình, Nam Từ Liêm, Hà Nội</p>
                               </div>
                           </div>
                            
                            
                        </div>
                                                
                    </div>
                </div>
                              
            </div>
        </div>        
    </div>
</section>
<!-- Jquery Core Js --> 
<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 

<script src="assets/bundles/knob.bundle.js"></script> <!-- Jquery Knob Plugin Js -->

<script src="assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js -->
<script src="assets/js/pages/charts/jquery-knob.js"></script>
</body>

<!-- Mirrored from thememakker.com/templates/oreo/hospital/html/light/profile.php by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Apr 2019 15:00:59 GMT -->
</html>