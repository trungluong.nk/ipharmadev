<?php include './header.php'; ?>
<!-- Main Content -->
<section class="content home">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12">
                <h2>Dashboard

                    <small>Welcome to iPharma</small>
                </h2>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-12 text-right">
                
                <button class="btn btn-white btn-icon btn-round hidden-sm-down float-right m-l-10" type="button">
                    <i class="zmdi zmdi-plus"></i>
                </button>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item">
                        <a href="index.php">
                            <i class="zmdi zmdi-home"></i> Trang chủ
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-4 col-md-6">
                <div class="card">
                    <div class="body">
                        <h3 class="number count-to m-b-0" data-from="0" data-to="1600" data-speed="2500" data-fresh-interval="700">1600 
                            <i class="zmdi zmdi-trending-up float-right"></i>
                        </h3>
                        <p class="text-muted">Số Lượng đơn hàng</p>
                        <div class="progress">
                            <div class="progress-bar l-blush" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100" style="width: 68%;"></div>
                        </div>
                        <small>Tiến độ 15%</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="card">
                    <div class="body">
                        <h3 class="number count-to m-b-0" data-from="0" data-to="3218" data-speed="2500" data-fresh-interval="1000">3218 
                            <i class="zmdi zmdi-trending-up float-right"></i>
                        </h3>
                        <p class="text-muted">Đơn hàng đã xử lý</p>
                        <div class="progress">
                            <div class="progress-bar l-green" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100" style="width: 68%;"></div>
                        </div>
                        <small>Tiến độ 23%</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12">
                <div class="card">
                    <div class="body">
                        <h3 class="number count-to m-b-0" data-from="0" data-to="284" data-speed="2500" data-fresh-interval="1000">284 
                            <i class="zmdi zmdi-trending-up float-right"></i>
                        </h3>
                        <p class="text-muted">Số lượng thuốc tồn kho 
                            <i class="zmdi zmdi-mood"></i>
                        </p>
                        <div class="progress">
                            <div class="progress-bar l-parpl" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100" style="width: 68%;"></div>
                        </div>
                        <small>Khối lượng 50%</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-8 col-md-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            <strong>Doanh số bán hàng </strong> Nhà thuốc iPharma 
                        </h2>
                        <ul class="header-dropdown">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="zmdi zmdi-more"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right slideUp float-right">
                                    <li>
                                        <a href="javascript:void(0);">Edit</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Delete</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Report</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="remove">
                                <a role="button" class="boxs-close">
                                    <i class="zmdi zmdi-close"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs padding-0">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#chart-view">Xem biểu đồ</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#table-view">Bảng dữ liệu</a>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content m-t-10">
                            <div class="tab-pane active" id="chart-view">
                                <div id="area_chart" class="graph"></div>
                                <div class="xl-slategray">
                                    <div class="body">
                                        <div class="row text-center">
                                            <div class="col-sm-3 col-6">
                                                <h4 class="margin-0">106 triệu</h4>
                                                <p class="text-muted margin-0"> Hôm nay</p>
                                            </div>
                                            <div class="col-sm-3 col-6">
                                                <h4 class="margin-0">907 triệu</h4>
                                                <p class="text-muted margin-0">Tuần này</p>
                                            </div>
                                            <div class="col-sm-3 col-6">
                                                <h4 class="margin-0">4,2 Tỷ</h4>
                                                <p class="text-muted margin-0">Tháng này</p>
                                            </div>
                                            <div class="col-sm-3 col-6">
                                                <h4 class="margin-0">7,5 Tỷ</h4>
                                                <p class="text-muted margin-0">Năm 2019</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="table-view">
                                <div class="table-responsive">
                                    <table class="table m-b-0 table-hover">
                                        <thead>
                                            <tr>
                                                <th>Tên nhà thuốc</th>
                                                <th class="hidden-sm-down">Địa chỉ</th>
                                                <th>Thu Nhập</th>
                                                <th class="hidden-md-down">Đánh giá</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Hospital Name</td>
                                                <td class="hidden-sm-down">Porterfield 508 Virginia Street Chicago, IL 60653</td>
                                                <td>$2,325</td>
                                                <td class="hidden-md-down">
                                                    <i class="zmdi zmdi-star col-amber"></i>
                                                    <i class="zmdi zmdi-star col-amber"></i>
                                                    <i class="zmdi zmdi-star col-amber"></i>
                                                    <i class="zmdi zmdi-star col-amber"></i>
                                                    <i class="zmdi zmdi-star col-amber"></i>
                                                </td>
                                                <td>
                                                    <button class="btn btn-sm btn-neutral">
                                                        <i class="zmdi zmdi-chart"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Hospital Name</td>
                                                <td class="hidden-sm-down">2595 Pearlman Avenue Sudbury, MA 01776 </td>
                                                <td>$3,325</td>
                                                <td class="hidden-md-down">
                                                    <i class="zmdi zmdi-star col-amber"></i>
                                                    <i class="zmdi zmdi-star col-amber"></i>
                                                    <i class="zmdi zmdi-star col-amber"></i>
                                                    <i class="zmdi zmdi-star col-amber"></i>
                                                    <i class="zmdi zmdi-star col-amber"></i>
                                                </td>
                                                <td>
                                                    <button class="btn btn-sm btn-neutral">
                                                        <i class="zmdi zmdi-chart"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Hospital Name</td>
                                                <td class="hidden-sm-down">Porterfield 508 Virginia Street Chicago, IL 60653</td>
                                                <td>$5,021</td>
                                                <td class="hidden-md-down">
                                                    <i class="zmdi zmdi-star col-amber"></i>
                                                    <i class="zmdi zmdi-star col-amber"></i>
                                                    <i class="zmdi zmdi-star col-amber"></i>
                                                    <i class="zmdi zmdi-star col-amber"></i>
                                                    <i class="zmdi zmdi-star col-amber"></i>
                                                </td>
                                                <td>
                                                    <button class="btn btn-sm btn-neutral">
                                                        <i class="zmdi zmdi-chart"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Hospital Name</td>
                                                <td class="hidden-sm-down">508 Virginia Street Chicago, IL 60653</td>
                                                <td>$1,325</td>
                                                <td class="hidden-md-down">
                                                    <i class="zmdi zmdi-star col-amber"></i>
                                                    <i class="zmdi zmdi-star col-amber"></i>
                                                    <i class="zmdi zmdi-star col-amber"></i>
                                                    <i class="zmdi zmdi-star col-amber"></i>
                                                    <i class="zmdi zmdi-star-outline"></i>
                                                </td>
                                                <td>
                                                    <button class="btn btn-sm btn-neutral">
                                                        <i class="zmdi zmdi-chart"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Hospital Name</td>
                                                <td class="hidden-sm-down">1516 Holt Street West Palm Beach, FL 33401</td>
                                                <td>$2,325</td>
                                                <td class="hidden-md-down">
                                                    <i class="zmdi zmdi-star col-amber"></i>
                                                    <i class="zmdi zmdi-star col-amber"></i>
                                                    <i class="zmdi zmdi-star col-amber"></i>
                                                    <i class="zmdi zmdi-star col-amber"></i>
                                                    <i class="zmdi zmdi-star-outline"></i>
                                                </td>
                                                <td>
                                                    <button class="btn btn-sm btn-neutral">
                                                        <i class="zmdi zmdi-chart"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Hospital Name</td>
                                                <td class="hidden-sm-down">508 Virginia Street Chicago, IL 60653</td>
                                                <td>$2,325</td>
                                                <td class="hidden-md-down">
                                                    <i class="zmdi zmdi-star col-amber"></i>
                                                    <i class="zmdi zmdi-star col-amber"></i>
                                                    <i class="zmdi zmdi-star col-amber"></i>
                                                    <i class="zmdi zmdi-star col-amber"></i>
                                                    <i class="zmdi zmdi-star-outline"></i>
                                                </td>
                                                <td>
                                                    <button class="btn btn-sm btn-neutral">
                                                        <i class="zmdi zmdi-chart"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12">
                <div class="card">
                    <div class="body">
                        <h6 class="text-center m-b-15">Danh sách đơn hàng</h6>
                        <div class="table-responsive m-t-20">
                            <table class="table table-striped m-b-0">
                                <thead>
                                    <tr>
                                        <th>Mã đơn</th>
                                        <th>Trị giá</th>
                                        <th>Thao tác</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>BM_007676</td>
                                        <td>215.000.000 vnđ</td>
                                        <td>Chi tiết</td>
                                    </tr>
                                    <tr>
                                        <td>BM_007677</td>
                                        <td>346.000.000 vnđ</td>
                                        <td>Chi tiết</td>
                                    </tr>
                                    <tr>
                                        <td>BM_007678</td>
                                        <td>123.000.000 vnđ</td>
                                        <td>Chi tiết</td>
                                    </tr>
                                    <tr>
                                        <td>BM_007679</td>
                                        <td>456.000.000 vnđ</td>
                                        <td>Chi tiết</td>
                                    </tr>
                                    <tr>
                                        <td>BM_007680</td>
                                        <td>235.000.000 vnđ</td>
                                        <td>Chi tiết</td>
                                    </tr>
                                    <tr>
                                        <td>BM_007681</td>
                                        <td>456.000.000 vnđ</td>
                                        <td>Chi tiết</td>
                                    </tr>
                                    <tr>
                                        <td>BM_007682</td>
                                        <td>455.000.000 vnđ</td>
                                        <td>Chi tiết</td>
                                    </tr>
                                    <tr>
                                        <td>BM_007683</td>
                                        <td>415.000.000 vnđ</td>
                                        <td>Chi tiết</td>
                                    </tr>
                                    <tr>
                                        <td>BM_007684</td>
                                        <td>565.000.000 vnđ</td>
                                        <td>Chi tiết</td>
                                    </tr>
                                    <tr>
                                        <td>BM_007685</td>
                                        <td>345.000.000 vnđ</td>
                                        <td>Chi tiết</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-4 col-md-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            <strong>Lịch làm việc</strong>
                        </h2>
                        <ul class="header-dropdown">
                            <li class="remove">
                                <a role="button" class="boxs-close">
                                    <i class="zmdi zmdi-close"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="new_timeline">
                            <div class="header">
                                <div class="color-overlay">
                                    <div class="day-number">20</div>
                                    <div class="date-right">
                                        <div class="day-name">Thứ 2</div>
                                        <div class="month">Tháng 4 2019</div>
                                    </div>
                                </div>
                            </div>
                            <ul>
                                <li>
                                    <div class="bullet pink"></div>
                                    <div class="time">5pm</div>
                                    <div class="desc">
                                        <h3>Giao ban</h3>
                                        <h4>Mobile App</h4>
                                    </div>
                                </li>
                                <li>
                                    <div class="bullet green"></div>
                                    <div class="time">3 - 4pm</div>
                                    <div class="desc">
                                        <h3>Thu thập dữ liệu</h3>
                                        <h4>Hangouts</h4>
                                    </div>
                                </li>
                                <li>
                                    <div class="bullet orange"></div>
                                    <div class="time">12pm</div>
                                    <div class="desc">
                                        <h3>Lunch Break</h3>
                                    </div>
                                </li>
                                <li>
                                    <div class="bullet green"></div>
                                    <div class="time">9 - 11am</div>
                                    <div class="desc">
                                        <h3>bàn giao</h3>
                                        <h4>Web App</h4>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-12">
                <div class="row clearfix">
                    <div class="col-lg-4 col-md-6">
                        <div class="card top_counter">
                            <div class="body">
                                <div class="icon xl-slategray">
                                    <i class="zmdi zmdi-account"></i>
                                </div>
                                <div class="content">
                                    <div class="text">Nhà thuốc đã kết nối</div>
                                    <h5 class="number">2758</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="card top_counter">
                            <div class="body">
                                <div class="icon xl-slategray">
                                    <i class="zmdi zmdi-account"></i>
                                </div>
                                <div class="content">
                                    <div class="text">Nhà thuốc đang hoạt động</div>
                                    <h5 class="number">1985</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="card top_counter">
                            <div class="body">
                                <div class="icon xl-slategray">
                                    <i class="zmdi zmdi-bug"></i>
                                </div>
                                <div class="content">
                                    <div class="text">Nhà thuốc không hoạt động</div>
                                    <h5 class="number">200</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card visitors-map">
                    <div class="header">
                        <h2>
                            <strong>Danh sách chi nhánh</strong>  
                            <small>Dịa chỉ chi nhanh</small>
                        </h2>
                        <ul class="header-dropdown">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="zmdi zmdi-more"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right slideUp">
                                    <li>
                                        <a href="javascript:void(0);">Action</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Another action</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">Something else</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="remove">
                                <a role="button" class="boxs-close">
                                    <i class="zmdi zmdi-close"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="row">
                            <div class="col-lg-6 col-md-12">
                                <div id="" style="height:280px;">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14897.786215080107!2d105.83490099721591!3d21.01481102659443!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x73c533e965db8f9c!2zTmjDoCB0aHXhu5FjIE5n4buNYyBWaeG7h3Q!5e0!3m2!1svi!2s!4v1556570338469!5m2!1svi!2s" width="494" height="280" frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12">
                                <div class="body">
                                    <ul class="row location_list list-unstyled">
                                        <li class="col-lg-4 col-md-4 col-6">
                                            <div class="body xl-turquoise">
                                                <i class="zmdi zmdi-pin"></i>
                                                <h4 class="number count-to" data-from="0" data-to="453" data-speed="2500" data-fresh-interval="700">453</h4>
                                                <span>Hà Nội</span>
                                            </div>
                                        </li>
                                        <li class="col-lg-4 col-md-4 col-6">
                                            <div class="body xl-khaki">
                                                <i class="zmdi zmdi-pin"></i>
                                                <h4 class="number count-to" data-from="0" data-to="124" data-speed="2500" data-fresh-interval="700">124</h4>
                                                <span>Ninh Bình</span>
                                            </div>
                                        </li>
                                        <li class="col-lg-4 col-md-4 col-6">
                                            <div class="body xl-parpl">
                                                <i class="zmdi zmdi-pin"></i>
                                                <h4 class="number count-to" data-from="0" data-to="215" data-speed="2500" data-fresh-interval="700">215</h4>
                                                <span>Thanh Hóa</span>
                                            </div>
                                        </li>
                                        <li class="col-lg-4 col-md-4 col-6">
                                            <div class="body xl-salmon">
                                                <i class="zmdi zmdi-pin"></i>
                                                <h4 class="number count-to" data-from="0" data-to="155" data-speed="2500" data-fresh-interval="700">155</h4>
                                                <span>Đà Nẵng</span>
                                            </div>
                                        </li>
                                        <li class="col-lg-4 col-md-4 col-6">
                                            <div class="body xl-blue">
                                                <i class="zmdi zmdi-pin"></i>
                                                <h4 class="number count-to" data-from="0" data-to="78" data-speed="2500" data-fresh-interval="700">78</h4>
                                                <span>HCM</span>
                                            </div>
                                        </li>
                                        <li class="col-lg-4 col-md-4 col-6">
                                            <div class="body xl-slategray">
                                                <i class="zmdi zmdi-pin"></i>
                                                <h4 class="number count-to" data-from="0" data-to="55" data-speed="2500" data-fresh-interval="700">55</h4>
                                                <span>Other</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-4 col-md-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            <strong>Thuốc phẫu thuật tim</strong>  
                            <small>Tăng 18% so với tháng trước</small>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="sparkline" data-type="line" data-spot-Radius="1" data-highlight-Spot-Color="rgb(233, 30, 99)" data-highlight-Line-Color="#222"
                             data-min-Spot-Color="rgb(233, 30, 99)" data-max-Spot-Color="rgb(96, 125, 139)" data-spot-Color="rgb(96, 125, 139, 0.7)"
                             data-offset="90" data-width="100%" data-height="50px" data-line-Width="1" data-line-Color="rgb(96, 125, 139, 0.7)"
                             data-fill-Color="rgba(96, 125, 139, 0.3)"> 6,4,7,8,4,3,2,2,5,6,7,4,1,5,7,9,9,8,7,6 </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            <strong>Kết nối nhà thuốc</strong>
                            <small>Tăng 28% so với tháng trước</small>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="sparkline" data-type="line" data-spot-Radius="1" data-highlight-Spot-Color="rgb(233, 30, 99)" data-highlight-Line-Color="#222"
                             data-min-Spot-Color="rgb(233, 30, 99)" data-max-Spot-Color="rgb(120, 184, 62)" data-spot-Color="rgb(120, 184, 62, 0.7)"
                             data-offset="90" data-width="100%" data-height="50px" data-line-Width="1" data-line-Color="rgb(120, 184, 62, 0.7)"
                             data-fill-Color="rgba(120, 184, 62, 0.3)"> 6,4,7,6,9,3,3,5,7,4,2,3,7,6 </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            <strong>Thu thập dữ liệu </strong>
                            <small >Tăng 10% so với tháng trước</small>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="sparkline" data-type="bar" data-width="97%" data-height="50px" data-bar-Width="4" data-bar-Spacing="10" data-bar-Color="#00ced1">2,8,5,3,1,7,9,5,6,4,2,3,1</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-4 col-md-12">
                <div class="card tasks_report">
                    <div class="header">
                        <h2>
                            <strong>Tổng</strong> Doanh Thu
                        </h2>
                        <ul class="header-dropdown">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="zmdi zmdi-more"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right slideUp">
                                    <li>
                                        <a href="javascript:void(0);">năm 2019 </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">năm 2018 </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">năm 2017 </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="remove">
                                <a role="button" class="boxs-close">
                                    <i class="zmdi zmdi-close"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="body text-center">
                        <h4 class="margin-0">Doanh số</h4>
                        <h6 class="m-b-20">2,45,124</h6>
                        <input type="text" class="knob dial1" value="66" data-width="100" data-height="100" data-thickness="0.1" data-fgColor="#212121" readonly>
                        <h6 class="m-t-20">Dánh giá</h6>
                        <small class="displayblock">47%  
                            <i class="zmdi zmdi-trending-up"></i>
                        </small>
                        <div class="sparkline m-t-20" data-type="bar" data-width="97%" data-height="28px" data-bar-Width="2" data-bar-Spacing="8" data-bar-Color="#212121">3,2,6,5,9,8,7,8,4,5,1,2,9,5,1,3,5,7,4,6</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-12">
                <div class="card patient_list">
                    <div class="header">
                        <h2>
                            <strong>Danh sách </strong> Nhà Thuốc mới
                        </h2>
                        <ul class="header-dropdown">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="zmdi zmdi-more"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right slideUp">
                                    <li>
                                        <a href="javascript:void(0);">2017 Year</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">2016 Year</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">2015 Year</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="remove">
                                <a role="button" class="boxs-close">
                                    <i class="zmdi zmdi-close"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-striped m-b-0">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th></th>
                                        <th>Tên</th>
                                        <th>Địa chỉ</th>
                                        <th>Trạng thái</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>
                                            <img src="http://via.placeholder.com/35x35" alt="Avatar" class="rounded-circle">
                                        </td>
                                        <td>Nhà thuốc iPharma</td>
                                        <td>123 6th St. Melbourne, FL 32904</td>
                                        <td>
                                            <span class="badge badge-danger">Dừng hoạt động</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>
                                            <img src="http://via.placeholder.com/35x35" alt="Avatar" class="rounded-circle">
                                        </td>
                                        <td>Nhà thuốc Đồng Tâm </td>
                                        <td>71 Pilgrim Avenue Chevy Chase, MD 20815</td>
                                        <td>
                                            <span class="badge badge-info">Tiếp nhận hồ sơ</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>
                                            <img src="http://via.placeholder.com/35x35" alt="Avatar" class="rounded-circle">
                                        </td>
                                        <td>Woods</td>
                                        <td>70 Bowman St. South Windsor, CT 06074</td>
                                        <td>
                                            <span class="badge badge-warning">chờ duỵet</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>
                                            <img src="http://via.placeholder.com/35x35" alt="Avatar" class="rounded-circle">
                                        </td>
                                        <td>Nhà thuốc Kim Liên 1</td>
                                        <td>4 Goldfield Rd.Honolulu, HI 96815</td>
                                        <td>
                                            <span class="badge badge-success">Hoạt động</span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Jquery Core Js -->
<script src="assets/bundles/libscripts.bundle.js"></script>
<!-- Lib Scripts Plugin Js ( jquery.v3.2.1, Bootstrap4 js) -->
<script src="assets/bundles/vendorscripts.bundle.js"></script>
<!-- slimscroll, waves Scripts Plugin Js -->
<script src="assets/bundles/morrisscripts.bundle.js"></script>
<!-- Morris Plugin Js -->
<script src="assets/bundles/jvectormap.bundle.js"></script>
<!-- JVectorMap Plugin Js -->
<script src="assets/bundles/knob.bundle.js"></script>
<!-- Jquery Knob, Count To, Sparkline Js -->
<script src="assets/bundles/mainscripts.bundle.js"></script>
<script src="assets/js/pages/index.js"></script>
</body>
<!-- Mirrored from thememakker.com/templates/oreo/hospital/html/light/index.php by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Apr 2019 15:00:40 GMT -->
</html>