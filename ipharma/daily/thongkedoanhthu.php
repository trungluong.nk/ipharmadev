<?php include './header.php'; ?>
<!-- Main Content -->
<section class="content home">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12">
                <h2>Thống kê doanh thu
                <small>Welcome to iPhama</small>
                </h2>
            </div>            
            <div class="col-lg-7 col-md-7 col-sm-12 text-right">
                <div class="inlineblock text-center m-r-15 m-l-15 hidden-md-down">
                    
                </div>
                <div class="inlineblock text-center m-r-15 m-l-15 hidden-md-down">
                   
                </div>
                <button class="btn btn-white btn-icon btn-round hidden-sm-down float-right m-l-10" type="button">
                    <i class="zmdi zmdi-plus"></i>
                </button>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-home"></i> iPharma</a></li>
                    <li class="breadcrumb-item active">Thống kê doanh thu</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
         <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                       
                        <ul class="header-dropdown">
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        
                        
                    </div>
                    <div class="tab-content m-t-10">
                            <div class="tab-pane table-responsive active" id="All">
                                <h4 style="padding-left: 10px;">Ngày hôm nay</h4>
                                <table class="table table-hover" style="font-size: 10px;">
                                    <thead>
                                        <tr>                                       
                                            <th>THÁNG NÀY</th>
                                            <th>THỰC TẾ</th>
                                           
                                            <th>KẾ HOẠCH</th>
                                            <th>CHÊNH LỆCH %</th>
                                            <th>SỐ THỰC TẾ TÍNH ĐẾN HÔM NAY</th>
                                            <th>SỐ THEO KẾ HOẠCH TÍNH ĐẾN HÔM NAY</th>
                                            <th>SỐ CHÊNH LỆCH TÍNH ĐẾN HÔM NAY</th>
                                            <th>% TÍNH ĐẾN HÔM NAY (YTD)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>                                 
                                            <td>Số lượng</td>
                                            <td>50000</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>                                 
                                            <td>Bán hàng</td>
                                            <td>50000</td>
                                            <td>5000</td>
                                            <td>5000</td>
                                            <td>5000</td>
                                            <td></td>
                                            <td>
                                                
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>                                 
                                            <td>Doanh thu</td>
                                            <td>50000</td>
                                            <td>5000</td>
                                            <td>5000</td>
                                            <td>5000</td>
                                            <td></td>
                                            <td>
                                                
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>                                 
                                            <td>Biên</td>
                                            <td>50000</td>
                                            <td>5000</td>
                                            <td>5000</td>
                                            <td>5000</td>
                                            <td></td>
                                            <td>
                                                
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>                                 
                                            <td>Số Đơn hàng</td>
                                            <td>50000</td>
                                            <td>5000</td>
                                            <td>5000</td>
                                            <td>5000</td>
                                            <td></td>
                                            <td>
                                                
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>                                 
                                            <td>Giá trị Đơn hàng trung bình</td>
                                            <td>50000</td>
                                            <td>5000</td>
                                            <td>5000</td>
                                            <td>5000</td>
                                            <td></td>
                                            <td>
                                                
                                            </td>
                                            <td></td>
                                        </tr>

                                    </tbody>
                                </table>
                                <table class="table table-hover" style="font-size: 10px;">
                                    <thead>
                                        <tr>
                                            <th>THÁNG TỚI</th>
                                            <th>THÁNG TỚI</th>
                                            <th>QUÝ TỚI</th>
                                            <th>NĂM TỚI</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Bán hàng</td>
                                            <td>5000</td>
                                            <td>5000</td>
                                            <td>5000</td>
                                        </tr>
                                    </tbody>
                                </table>                            
                            </div>
                           
                        </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                       
                        <ul class="header-dropdown">
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div id="chartContainer1" style="height: 300px; width: 100%;"></div>
                        
                    </div>
                   
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                       
                        <ul class="header-dropdown">
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="row">
                            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                                <div id="chartContainer2" style="height: 300px; width: 100%;"></div>
                            </div>
                            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                                <div id="chartContainer3" style="height: 300px; width: 100%;"></div>
                            </div>
                        </div>
                        
                    </div>
                   
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                       
                        <ul class="header-dropdown">
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="row">
                            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                                <div id="chartContainer4" style="height: 300px; width: 100%;"></div>
                            </div>
                            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                                <div id="chartContainer5" style="height: 300px; width: 100%;"></div>
                            </div>
                        </div>
                        
                    </div>
                   
                </div>
            </div>
        </div>
                  
    </div>



</section>

<!-- Central Modal Small -->

<!-- Central Modal Small -->



<!-- Jquery Core Js --> 


<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Bootstrap JS and jQuery v3.2.1 -->
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- slimscroll, waves Scripts Plugin Js -->  

<script src="plugins/dropzone/dropzone.js"></script> <!-- Dropzone Plugin Js -->
<script src="plugins/momentjs/moment.js"></script> <!-- Moment Plugin Js -->
<script src="plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<script src="assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js -->
<script>
    $(function () {
    //Datetimepicker plugin
    $('.datetimepicker').bootstrapMaterialDatePicker({
        format: 'dddd DD MMMM YYYY - HH:mm',
        clearButton: true,
        weekStart: 1
    });
});
</script>
<script type="text/javascript">
    window.onload = function () {
        var chart1 = new CanvasJS.Chart("chartContainer1", {
    animationEnabled: true,
    
    title:{
        text: "Lịch sử bán hàng",
        fontFamily: "tahoma",
        verticalAlign: "top",
        horizontalAlign: "left",    
         fontSize : 15           
    }, 
    axisY:{
        title: "",
         interval: 50000000,
        maximum: 250000000,
        minimum: 5000000,

    },
    toolTip: {
        shared: true
    },
    legend: {
        horizontalAlign: "right",
        verticalAlign: "top"
      },
    data: [{        
       type: "spline",  
        name: "Số tiền",        
        showInLegend: true,
        dataPoints: [
             { label: "23/3/1019" , y: 49000000 },     
            { label:"23/4/1019", y: 56000000 },     
            { label: "23/5/1019", y: 65000000 },     
            { label: "23/6/1019", y: 76000000 },     
            { label: "23/7/1019", y: 86000000 },
            { label: "23/8/1019", y: 190000000 }
        ]
    },
    {        
        type: "spline",  
        name: "Theo kế hoạch",        
        showInLegend: true,
        dataPoints: [
             { label: "23/3/1019" , y: 60000000 },     
            { label:"23/4/1019", y: 70000000 },     
            { label: "23/5/1019", y: 75000000 },     
            { label: "23/6/1019", y: 85000000 },     
            { label: "23/7/1019", y: 90000000 },
            { label: "23/8/1019", y: 180000000 }
        ]
    },
    {        
        type: "spline",  
        name: "Chi phí",        
        showInLegend: true,
        dataPoints: [
           { label: "23/3/1019" , y: 56000000 },     
            { label:"23/4/1019", y: 58000000 },     
            { label: "23/5/1019", y: 65000000 },     
            { label: "23/6/1019", y: 79000000 },     
            { label: "23/7/1019", y: 150000000 },
            { label: "23/8/1019", y: 200000000 }
        ]
    },
    
    {        
        type: "spline",  
        name: "Doanh thu",        
        showInLegend: true,
        dataPoints: [
            { label: "23/3/1019" , y: 70000000 },     
            { label:"23/4/1019", y: 75000000 },     
            { label: "23/5/1019", y: 85000000 },     
            { label: "23/6/1019", y: 90000000 },     
            { label: "23/7/1019", y: 100000000 },
            { label: "23/8/1019", y: 150000000 }
        ]
    }]
});

chart1.render();

var chart2 = new CanvasJS.Chart("chartContainer2", {

    animationEnabled: true,
    
    title:{
        text: " DỰ BÁO THÁNG",
        fontFamily: "tahoma",
        verticalAlign: "top",
        horizontalAlign: "left",     
         fontSize : 15          
    }, 
    axisY:{
        title: "",
         interval: 500000000,
        maximum: 1000000000,
        minimum: 0,
    },
    toolTip: {
        shared: true
    },
    legend: {
        horizontalAlign: "right",
        verticalAlign: "top"
      },
    data: [{        
       type: "spline",  
        name: "Số tiền",        
        showInLegend: false,
        dataPoints: [
             { label: "23/3/1019" , y: 490000000 },     
            { label:"23/4/1019", y: 560000000 },     
            { label: "23/5/1019", y: 650000000 },     
            { label: "23/6/1019", y: 760000000 },     
            { label: "23/7/1019", y: 860000000 },
            { label: "23/8/1019", y: 900000000 }
        ]
    }]
});

chart2.render();

var chart3 = new CanvasJS.Chart("chartContainer3", {

    animationEnabled: true,
    
    title:{
        text: " DỰ BÁO QUÝ",
        fontFamily: "tahoma",
        verticalAlign: "top",
        horizontalAlign: "left", 
        fontSize : 15            
    }, 
    axisY:{
        title: "",
         interval: 500000000,
        maximum: 1000000000,
        minimum: 0,
    },
    toolTip: {
        shared: true
    },
    legend: {
        horizontalAlign: "right",
        verticalAlign: "top"
      },
    data: [{        
       type: "spline",  
        name: "Số tiền",        
        showInLegend: false,
        dataPoints: [
             { label: "23/3/1019" , y: 490000000 },     
            { label:"23/4/1019", y: 560000000 },     
            { label: "23/5/1019", y: 650000000 },     
            { label: "23/6/1019", y: 760000000 },     
            { label: "23/7/1019", y: 860000000 },
            { label: "23/8/1019", y: 900000000 }
        ]
    }]
});

chart3.render();

var chart4 = new CanvasJS.Chart("chartContainer4", {

    animationEnabled: true,
    
    title:{
        text: " DỰ BÁO NĂM",
        fontFamily: "tahoma",
        verticalAlign: "top",
        horizontalAlign: "left", 
        fontSize : 15            
    }, 
    axisY:{
        title: "",
         interval: 500000000,
        maximum: 1000000000,
        minimum: 0,
    },
    toolTip: {
        shared: true
    },
    legend: {
        horizontalAlign: "right",
        verticalAlign: "top"
      },
    data: [{        
       type: "spline",  
        name: "Số tiền",        
        showInLegend: false,
        dataPoints: [
             { label: "23/3/1019" , y: 490000000 },     
            { label:"23/4/1019", y: 560000000 },     
            { label: "23/5/1019", y: 650000000 },     
            { label: "23/6/1019", y: 760000000 },     
            { label: "23/7/1019", y: 860000000 },
            { label: "23/8/1019", y: 900000000 }
        ]
    }]
});

chart4.render();

var chart5 = new CanvasJS.Chart("chartContainer5", {

    animationEnabled: true,
    
    title:{
        text: " DÒNG DOANH THU",
        fontFamily: "tahoma",
        verticalAlign: "top",
        horizontalAlign: "left", 
        fontSize : 15            
    }, 
    axisY:{
        title: "",
         interval: 500000000,
        maximum: 1000000000,
        minimum: 0,
    },
    toolTip: {
        shared: true
    },
    legend: {
        horizontalAlign: "right",
        verticalAlign: "top"
      },
    data: [{        
       type: "spline",  
        name: "Số tiền",        
        showInLegend: false,
        dataPoints: [
             { label: "23/3/1019" , y: 490000000 },     
            { label:"23/4/1019", y: 560000000 },     
            { label: "23/5/1019", y: 650000000 },     
            { label: "23/6/1019", y: 760000000 },     
            { label: "23/7/1019", y: 860000000 },
            { label: "23/8/1019", y: 900000000 }
        ]
    }]
});

chart5.render();

function toggleDataSeries(e) {
}

    }
</script>
</body>

<!-- Mirrored from thememakker.com/templates/oreo/hospital/html/light/index.php by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Apr 2019 15:00:40 GMT -->
</html>