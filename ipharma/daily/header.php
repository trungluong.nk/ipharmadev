<!doctype html><html class="no-js " lang="en"><head><meta charset="utf-8"><meta http-equiv="X-UA-Compatible" content="IE=Edge"><meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"><meta name="description" content="Responsive Bootstrap 4 and web Application ui kit."><title>iPharma</title><link rel="icon" href="favicon.ico" type="image/x-icon">
        <!-- Favicon-->
        <link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="plugins/dropzone/dropzone.css">
        <link href="plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
        <link rel="stylesheet" href="plugins/bootstrap-select/css/bootstrap-select.css"/>
        <!-- Custom Css -->
        <link rel="stylesheet" href="assets/css/main.css">
        <link rel="stylesheet" href="assets/css/color_skins.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    </head>
    <body class="theme-cyan">
        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <!--<div class="loader">-->
            <div class="m-t-30">
                <img class="zmdi-hc-spin" src="images/logo.png" width="48" height="48" alt="iPharma">
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- Top Bar -->
    <nav class="navbar p-l-5 p-r-5">
        <ul class="nav navbar-nav navbar-left">
            <li>
                <div class="navbar-header">
                    <a href="javascript:void(0);" class="bars"></a>
                    <a class="navbar-brand" href="index.php">
                        <img src="images/logo.png" width="" alt="iPharma">
                        <span class="m-l-10">iPharma</span>
                    </a>
                </div>
            </li>
            <li>
                <a href="javascript:void(0);" class="ls-toggle-btn" data-close="true">
                    <i class="zmdi zmdi-swap"></i>
                </a>
            </li>
            
            <li class="dropdown">
                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                    <i class="zmdi zmdi-notifications"></i>
                    <div class="notify">
                        <span class="heartbit"></span>
                        <span class="point"></span>
                    </div>
                </a>

            </li>
            <li class="dropdown">
                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                    <i class="zmdi zmdi-flag"></i>
                    <div class="notify">
                        <span class="heartbit"></span>
                        <span class="point"></span>
                    </div>
                </a>
                <ul class="dropdown-menu pullDown">

                </ul>
            </li>
            <li class="hidden-sm-down">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Tìm Kiếm...">
                    <span class="input-group-addon">
                        <i class="zmdi zmdi-search"></i>
                    </span>
                </div>
            </li>
            <li class="float-right">
                <a href="sign-in.php" class="mega-menu" data-close="true">
                    <i class="zmdi zmdi-power"></i>
                </a>
               
            </li>
        </ul>
    </nav>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#dashboard">
                    <i class="zmdi zmdi-home m-r-5"></i>Home
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#user">iPharma</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane stretchRight active" id="dashboard">
                <div class="menu">
                    <ul class="list">
                        <li>
                            <div class="user-info">
                                <div class="image"><a href="profile.php"><img src="images/profile_av.jpg" alt="User"></a></div>
                                <div class="detail">
                                    <h4>Đại lý iPharma</h4>
                                </div>
                            </div>
                        </li>
                        <li class="active open">
                            <a href="index.php">
                                <i class="zmdi zmdi-home"></i>
                                <span>Trang chủ</span>
                            </a>
                        </li>
                        <li>
                            <a href="profile.php">
                                <i class="zmdi zmdi-label-alt"></i>
                                <span>Profile</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="zmdi zmdi-account-add"></i>
                                <span>Quản lý quảng cáo</span>
                            </a>
                            <ul class="ml-menu">
                                <li>
                                    <a href="quanlyquangcao.php">Quản lý quảng cáo</a>
                                </li>
                                <li>
                                    <a href="#">Quản lý thanh toán quảng cáo</a>
                                </li>
                                
                            </ul>
                        </li>
                        <li>
                            <a href="quanlynhathuoc.php">
                                <i class="zmdi zmdi-label-alt"></i>
                                <span>Quản lý nhà thuốc</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="zmdi zmdi-label-alt"></i>
                                <span>Quản lý Cộng tác viên</span>
                            </a>
                        </li>
                        <li>
                            <a href="quanlydonhang.php">
                                <i class="zmdi zmdi-label-alt"></i>
                                <span>Quản lý đơn hàng</span>
                            </a>
                        </li>
                        <li>
                            <a href="quanlyhoadon.php">
                                <i class="zmdi zmdi-label-alt"></i>
                                <span>Quản lý hóa đơn</span>
                            </a>
                        </li>
                        <li>
                            <a href="quanlynhacungcap.php">
                                <i class="zmdi zmdi-label-alt"></i>
                                <span>Quản lý nhà cung cấp</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="zmdi zmdi-account-o"></i>
                                <span>Báo cáo thống kê</span>
                            </a>
                            <ul class="ml-menu">
                                <li>
                                    <a href="thongkedoanhthu.php">Báo cáo doanh thu</a>
                                </li>
                                <li>
                                    <a href="baocaothuoc.php">Báo cáo thuốc</a>
                                </li>
                                <li>
                                    <a href="luongdonhang.php">Thống kê đơn hàng</a>
                                </li>
                                <!-- <li>
                                    <a href="quanlyhoadon.php">Quản lý hóa đơn</a>
                                </li>
                                <li>
                                    <a href="xulydonhang.php">Xử lý đơn hàng</a>
                                </li>
                                <li>
                                    <a href="#">Báo cáo đơn hàng</a>
                                </li> -->
                            </ul>
                        </li>
                       <!--  <li>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="zmdi zmdi-account-add"></i>
                                <span>Quản lý đối tác</span>
                            </a>
                            <ul class="ml-menu">
                                <li>
                                    <a href="quanlydanhmucdoitac.php">Quản lý Danh mục đối tác</a>
                                </li>
                                <li>
                                    <a href="quanlydoitac.php">Quản lý Đối tác</a>
                                </li>
                                <li>
                                    <a href="quanlylienhedoitac.php">Quản lý Liên hệ đối tác</a>
                                </li>
                                <li>
                                    <a href="phanquyendoitac.php">Phân quyền đối tác</a>
                                </li>
                            </ul>
                        </li> -->
                        <!-- <li>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="zmdi zmdi-account-o"></i>
                                <span>Quản lý đơn hàng</span>
                            </a>
                            <ul class="ml-menu">
                                <li>
                                    <a href="quanlydonhang.php">Quản lý đơn hàng</a>
                                </li>
                                <li>
                                    <a href="timkiemdonhang.php">Tìm kiếm đơn hàng</a>
                                </li>
                                <li>
                                    <a href="quanlydonhangcuadoitac.php">Quản lý đơn hàng của đối tác</a>
                                </li>
                                <li>
                                    <a href="quanlyhoadon.php">Quản lý hóa đơn</a>
                                </li>
                                <li>
                                    <a href="xulydonhang.php">Xử lý đơn hàng</a>
                                </li>
                                <li>
                                    <a href="#">Báo cáo đơn hàng</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="zmdi zmdi-account-o"></i>
                                <span>Quản lý thư viện thuốc</span>
                            </a>
                            <ul class="ml-menu">
                                <li>
                                    <a href="quanlydanhmuc.php">Quản lý danh mục</a>
                                </li>
                                <li>
                                    <a href="quanlynhacungcap.php">QUản lý nhà cung cấp</a>
                                </li>
                                <li>
                                    <a href="quanlythuoc.php">Quản lý thuốc</a>
                                </li>
                                <li>
                                    <a href="quanlythuoc.php">Tìm kiếm thuốc</a>
                                </li>
                                <li>
                                    <a href="quanlythuoc.php">Import dữ liệu thuốc</a>
                                </li>
                                <li>
                                    <a href="baocaothongkethuoc.php">Báo cáo thống kê thuốc</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="zmdi zmdi-label-alt"></i>
                                <span>Người dùng ứng dụng</span>
                            </a>
                            <ul class="ml-menu">
                                <li>
                                    <a href="quanlyroles.php">Quản lý Roles</a>
                                </li>
                                <li>
                                    <a href="quanlynguoidung.php">Quản lý người dùng</a>
                                </li>
                                <li>
                                    <a href="quanlyphanquyentheorole.php">Phân quyền người dùng</a>
                                </li>
                            </ul>
                        </li> -->
                        <li>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="zmdi zmdi-lock"></i>
                                <span>Tài khoản</span>
                            </a>
                            <ul class="ml-menu">
                                <li>
                                    <a href="sign-in.php">Đăng nhập</a>
                                </li>
                                <li>
                                    <a href="sign-up.php">Đăng kí</a>
                                </li>
                            </ul>
                        </li>
                       <!--  <li>
                            <a href="caidatthongsohethong.php">
                                <i class="zmdi zmdi-label-alt"></i>
                                <span>Cấu hình</span>
                            </a>
                        </li>
                         
                        <li>
                            <a href="thuthapthongtinnhathuoc.php">
                                <i class="zmdi zmdi-label-alt"></i>
                                <span>Thu thập thông tin nhà thuốc</span>
                            </a>
                        </li>
                         <li>
                            <a href="ketnoinhathuoc.php">
                                <i class="zmdi zmdi-label-alt"></i>
                                <span>Kết nối nhà thuốc</span>
                            </a>
                        </li>
                         <li>
                            <a href="hethongdanhgiaKPI.php">
                                <i class="zmdi zmdi-label-alt"></i>
                                <span>Hệ thống đánh giá KPI</span>
                            </a>
                        </li>
                        
                        <li>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="zmdi zmdi-label-alt"></i>
                                <span>Web Đại lý</span>
                            </a>
                            <ul class="ml-menu">
                                <li>
                                    <a href="tiepnhandonhang.php">Tiếp nhận đơn hàng</a>
                                </li>
                                <li>
                                    <a href="theodoidonhang.php">Theo dõi đơn hàng</a>
                                </li>
                                <li>
                                    <a href="quanlykhachhang.php">Quản lý khách hàng</a>
                                </li>
                                <li>
                                    <a href="quanlythanhtoan.php">Quản lý thanh toán</a>
                                </li>
                                <li>
                                    <a href="quanlyphanhoi.php">Quản lý phản hồi </a>
                                </li>
                                <li>
                                    <a href="baocaothuoc.php">Báo cáo thuốc</a>
                                </li>
                                <li>
                                    <a href="thongkedoanhthu.php">Báo cáo doanh thu</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="zmdi zmdi-label-alt"></i>
                                <span>Thống kê</span>
                            </a>
                            <ul class="ml-menu">
                                <li>
                                    <a href="luongnguoitruycap.php">Lượng người truy cập</a>
                                </li>
                                <li>
                                    <a href="luongdonhang.php">Lượng đơn hàng (thành công, thất bại)</a>
                                </li>
                                <li>
                                    <a href="loaithuocduocdatnhieunhat.php">Loại thuốc được đặt nhiều</a>
                                </li>
                                <li>
                                    <a href="Trendvebenhtrenhethong.php">Trend về bệnh trên hệ thống</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="zmdi zmdi-label-alt"></i>
                                <span>Báo cáo doanh thu</span>
                            </a>
                            <ul class="ml-menu">
                                <li>
                                    <a href="doanhthutheongaythangnam.php">Doanh thu theo ngày/tháng/năm</a>
                                </li>
                                <li>
                                    <a href="doanhthutheonhathuoc.php">Doanh thu theo nhà thuốc</a>
                                </li>
                                <li>
                                    <a href="doanhthutheodanhmucthuoc.php">Doanh thu theo danh mục thuốc</a>
                                </li>
                                <li>
                                    <a href="doanhthutheokhuvuc.php">Doanh thu theo khu vực</a>
                                </li>
                                <li>
                                    <a href="doanhthutheodotuoigioitinh.php">Doanh thu theo độ tuổi, giới tính</a>
                                </li>
                                <li>
                                    <a href="recommendation.php">Trang danh mục Recommendation</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="lichsugiaodichnganhang.php">
                                <i class="zmdi zmdi-label-alt"></i>
                                <span>Lịch sử giao dịch Ngân hàng</span>
                            </a>
                        </li>
                        <li>
                            <a href="lichsugiaodichgiaovan.php">
                                <i class="zmdi zmdi-label-alt"></i>
                                <span>Lịch sử giao dịch giao vận</span>
                            </a>
                        </li>
                        <li>
                            <a href="lichsugiaodich.php">
                                <i class="zmdi zmdi-label-alt"></i>
                                <span>Lịch sử giao dịch qua cổng thanh toán</span>
                            </a>
                        </li>
                        <li>
                            <a href="baocaotaichinh.php">
                                <i class="zmdi zmdi-label-alt"></i>
                                <span>Báo cáo tài chính</span>
                            </a>
                        </li>
                        
                        <li>
                            <a href="thongkeloinhuan.php">
                                <i class="zmdi zmdi-label-alt"></i>
                                <span>Thống kê lợi nhuận</span>
                            </a>
                        </li>
                        <li>
                            <a href="nhapdanhmucthuoc.php">
                                <i class="zmdi zmdi-label-alt"></i>
                                <span>Danh mục thuốc</span>
                            </a>
                        </li>
                        <li>
                            <a href="nhapdanhmucnhacungcap.php">
                                <i class="zmdi zmdi-label-alt"></i>
                                <span>Danh mục nhà cung cấp</span>
                            </a>
                        </li>
                        <li>
                            <a href="nhapdanhsachthuoc.php">
                                <i class="zmdi  zmdi-label-alt"></i>
                                <span>Nhập danh sách thuốc</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="zmdi zmdi-label-alt"></i>
                                <span>Nhà Quản lý</span>
                            </a>
                            <ul class="ml-menu">
                                <li>
                                    <a href="nqlkhachhang.php">Quản lý khách hàng</a>
                                </li>
                                <li>
                                    <a href="nqlquangcao.php">Quản lý quảng cáo</a>
                                </li>
                                <li>
                                    <a href="nqlthanhtoan.php">Quản lý thanh toán</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="zmdi zmdi-label-alt"></i>
                                <span>Khách hàng</span>
                            </a>
                            <ul class="ml-menu">
                                <li>
                                    <a href="quanlyquangcao.php">Quản lý quảng cáo</a>
                                </li>
                                <li>
                                    <a href="quanlythanhtoan.php">Quản lý thanh toán</a>
                                </li>
                            </ul>
                        </li> -->
                        <li>
                            <a href="bangxephang.php">
                                <i class="zmdi  zmdi-label-alt"></i>
                                <span>Bảng xếp hạng</span>
                            </a>
                        </li>
<!--                        <li>
                            <a href="index.php">
                                <i class="zmdi  zmdi-label-alt"></i>
                                <span>So sánh giá</span>
                            </a>
                        </li>-->
<!--                        <li>
                            <a href="index.php">
                                <i class="zmdi  zmdi-label-alt"></i>
                                <span>Đánh giá chất lượng</span>
                            </a>
                        </li>-->
                    </ul>
                </div>
            </div>
            <div class="tab-pane stretchLeft" id="user">
                <div class="menu">
                    <ul class="list">
                        <li>
                            <div class="user-info m-b-20 p-b-15">
                                <div class="image">
                                    <a href="profile.php">
                                        <img src="images/profile_av.jpg" alt="User">
                                    </a>
                                </div>
                                <div class="detail">
                                    <h4>Đại lý iPharma</h4>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <a title="facebook" href="#">
                                            <i class="zmdi zmdi-facebook"></i>
                                        </a>
                                        <a title="twitter" href="#">
                                            <i class="zmdi zmdi-twitter"></i>
                                        </a>
                                        <a title="instagram" href="#">
                                            <i class="zmdi zmdi-instagram"></i>
                                        </a>
                                    </div>
                                    <div class="col-4 p-r-0">
                                        <h5 class="m-b-5">18</h5>
                                        <small>Exp</small>
                                    </div>
                                    <div class="col-4">
                                        <h5 class="m-b-5">125</h5>
                                        <small>Awards</small>
                                    </div>
                                    <div class="col-4 p-l-0">
                                        <h5 class="m-b-5">148</h5>
                                        <small>Clients</small>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <small class="text-muted">Location: </small>
                            <p>795 Folsom Ave, Suite 600 San Francisco, CADGE 94107</p>
                            <hr>
                            <small class="text-muted">Email address: </small>
                            <p>Charlotte@example.com</p>
                            <hr>
                            <small class="text-muted">Phone: </small>
                            <p>+ 202-555-0191</p>
                            <hr>
                            <small class="text-muted">Website: </small>
                            <p>http://dr.charlotte.com/ </p>
                            <hr>
                            <ul class="list-unstyled">
                                <li>
                                    <div>Colorectal Surgery</div>
                                    <div class="progress m-b-20">
                                        <div class="progress-bar l-blue " role="progressbar" aria-valuenow="89" aria-valuemin="0" aria-valuemax="100" style="width: 89%">
                                            <span class="sr-only">62% Complete</span>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div>Endocrinology</div>
                                    <div class="progress m-b-20">
                                        <div class="progress-bar l-green " role="progressbar" aria-valuenow="56" aria-valuemin="0" aria-valuemax="100" style="width: 56%">
                                            <span class="sr-only">87% Complete</span>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div>Dermatology</div>
                                    <div class="progress m-b-20">
                                        <div class="progress-bar l-amber" role="progressbar" aria-valuenow="78" aria-valuemin="0" aria-valuemax="100" style="width: 78%">
                                            <span class="sr-only">32% Complete</span>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div>Neurophysiology</div>
                                    <div class="progress m-b-20">
                                        <div class="progress-bar l-blush" role="progressbar" aria-valuenow="43" aria-valuemin="0" aria-valuemax="100" style="width: 43%">
                                            <span class="sr-only">56% Complete</span>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </aside>
    <!-- Right Sidebar -->
    <aside id="rightsidebar" class="right-sidebar">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#setting">
                    <i class="zmdi zmdi-settings zmdi-hc-spin"></i>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#chat">
                    <i class="zmdi zmdi-comments"></i>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#activity">Activity</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane slideRight active" id="setting">
                <div class="slim_scroll">
                    <div class="card">
                        <h6>General Settings</h6>
                        <ul class="setting-list list-unstyled">
                            <li>
                                <div class="checkbox">
                                    <input id="checkbox1" type="checkbox">
                                    <label for="checkbox1">Report Panel Usage</label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox">
                                    <input id="checkbox2" type="checkbox" checked="">
                                    <label for="checkbox2">Email Redirect</label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox">
                                    <input id="checkbox3" type="checkbox" checked="">
                                    <label for="checkbox3">Notifications</label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox">
                                    <input id="checkbox4" type="checkbox" checked="">
                                    <label for="checkbox4">Auto Updates</label>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="card">
                        <h6>Skins</h6>
                        <ul class="choose-skin list-unstyled">
                            <li data-theme="purple">
                                <div class="purple"></div>
                            </li>
                            <li data-theme="blue">
                                <div class="blue"></div>
                            </li>
                            <li data-theme="cyan" class="active">
                                <div class="cyan"></div>
                            </li>
                            <li data-theme="green">
                                <div class="green"></div>
                            </li>
                            <li data-theme="orange">
                                <div class="orange"></div>
                            </li>
                            <li data-theme="blush">
                                <div class="blush"></div>
                            </li>
                        </ul>
                    </div>
                    <div class="card">
                        <h6>Account Settings</h6>
                        <ul class="setting-list list-unstyled">
                            <li>
                                <div class="checkbox">
                                    <input id="checkbox5" type="checkbox" checked="">
                                    <label for="checkbox5">Offline</label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox">
                                    <input id="checkbox6" type="checkbox" checked="">
                                    <label for="checkbox6">Location Permission</label>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="card theme-light-dark">
                        <h6>Left Menu</h6>
                        <button class="t-light btn btn-default btn-simple btn-round btn-block">Light</button>
                        <button class="t-dark btn btn-default btn-round btn-block">Dark</button>
                        <button class="m_img_btn btn btn-primary btn-round btn-block">Sidebar Image</button>
                    </div>
                    <div class="card">
                        <h6>Information Summary</h6>
                        <div class="row m-b-20">
                            <div class="col-7">
                                <small class="displayblock">MEMORY USAGE</small>
                                <h5 class="m-b-0 h6">512</h5>
                            </div>
                            <div class="col-5">
                                <div class="sparkline" data-type="bar" data-width="97%" data-height="25px" data-bar-Width="5" data-bar-Spacing="3" data-bar-Color="#00ced1">8,7,9,5,6,4,6,8</div>
                            </div>
                        </div>
                        <div class="row m-b-20">
                            <div class="col-7">
                                <small class="displayblock">CPU USAGE</small>
                                <h5 class="m-b-0 h6">90%</h5>
                            </div>
                            <div class="col-5">
                                <div class="sparkline" data-type="bar" data-width="97%" data-height="25px" data-bar-Width="5" data-bar-Spacing="3" data-bar-Color="#F15F79">6,5,8,2,6,4,6,4</div>
                            </div>
                        </div>
                        <div class="row m-b-20">
                            <div class="col-7">
                                <small class="displayblock">DAILY TRAFFIC</small>
                                <h5 class="m-b-0 h6">25 142</h5>
                            </div>
                            <div class="col-5">
                                <div class="sparkline" data-type="bar" data-width="97%" data-height="25px" data-bar-Width="5" data-bar-Spacing="3" data-bar-Color="#78b83e">7,5,8,7,4,2,6,5</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-7">
                                <small class="displayblock">DISK USAGE</small>
                                <h5 class="m-b-0 h6">60.10%</h5>
                            </div>
                            <div class="col-5">
                                <div class="sparkline" data-type="bar" data-width="97%" data-height="25px" data-bar-Width="5" data-bar-Spacing="3" data-bar-Color="#457fca">7,5,2,5,6,7,6,4</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane right_chat stretchLeft" id="chat">
                <div class="slim_scroll">
                    <div class="card">
                        <div class="search">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-addon">
                                    <i class="zmdi zmdi-search"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <h6>Recent</h6>
                        <ul class="list-unstyled">
                            <li class="online">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="images/xs/avatar4.jpg" alt="">
                                        <div class="media-body">
                                            <span class="name">Sophia</span>
                                            <span class="message">There are many variations of passages of Lorem Ipsum available</span>
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="online">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="images/xs/avatar5.jpg" alt="">
                                        <div class="media-body">
                                            <span class="name">Grayson</span>
                                            <span class="message">All the Lorem Ipsum generators on the</span>
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="offline">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="images/xs/avatar2.jpg" alt="">
                                        <div class="media-body">
                                            <span class="name">Isabella</span>
                                            <span class="message">Contrary to popular belief, Lorem Ipsum</span>
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="me">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="images/xs/avatar1.jpg" alt="">
                                        <div class="media-body">
                                            <span class="name">John</span>
                                            <span class="message">It is a long established fact that a reader</span>
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="online">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="images/xs/avatar3.jpg" alt="">
                                        <div class="media-body">
                                            <span class="name">Alexander</span>
                                            <span class="message">Richard McClintock, a Latin professor</span>
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="card">
                        <h6>Contacts</h6>
                        <ul class="list-unstyled">
                            <li class="offline inlineblock">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="images/xs/avatar10.jpg" alt="">
                                        <div class="media-body">
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="offline inlineblock">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="images/xs/avatar6.jpg" alt="">
                                        <div class="media-body">
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="offline inlineblock">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="images/xs/avatar7.jpg" alt="">
                                        <div class="media-body">
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="offline inlineblock">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="images/xs/avatar8.jpg" alt="">
                                        <div class="media-body">
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="offline inlineblock">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="images/xs/avatar9.jpg" alt="">
                                        <div class="media-body">
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="online inlineblock">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="images/xs/avatar5.jpg" alt="">
                                        <div class="media-body">
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="offline inlineblock">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="images/xs/avatar4.jpg" alt="">
                                        <div class="media-body">
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="offline inlineblock">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="images/xs/avatar3.jpg" alt="">
                                        <div class="media-body">
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="online inlineblock">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="images/xs/avatar2.jpg" alt="">
                                        <div class="media-body">
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="offline inlineblock">
                                <a href="javascript:void(0);">
                                    <div class="media">
                                        <img class="media-object " src="images/xs/avatar1.jpg" alt="">
                                        <div class="media-body">
                                            <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="tab-pane slideLeft" id="activity">
                <div class="slim_scroll">
                    <div class="card user_activity">
                        <h6>Recent Activity</h6>
                        <div class="streamline b-accent">
                            <div class="sl-item">
                                <img class="user rounded-circle" src="images/xs/avatar4.jpg" alt="">
                                <div class="sl-content">
                                    <h5 class="m-b-0">Admin Birthday</h5>
                                    <small>Jan 21 
                                        <a href="javascript:void(0);" class="text-info">Sophia</a>.
                                    </small>
                                </div>
                            </div>
                            <div class="sl-item">
                                <img class="user rounded-circle" src="images/xs/avatar5.jpg" alt="">
                                <div class="sl-content">
                                    <h5 class="m-b-0">Add New Contact</h5>
                                    <small>30min ago 
                                        <a href="javascript:void(0);">Alexander</a>.
                                    </small>
                                    <small>
                                        <strong>P:</strong> +264-625-2323
                                    </small>
                                    <small>
                                        <strong>E:</strong> maryamamiri@gmail.com
                                    </small>
                                </div>
                            </div>
                            <div class="sl-item">
                                <img class="user rounded-circle" src="images/xs/avatar6.jpg" alt="">
                                <div class="sl-content">
                                    <h5 class="m-b-0">Code Change</h5>
                                    <small>Today 
                                        <a href="javascript:void(0);">Grayson</a>.
                                    </small>
                                    <small>The standard chunk of Lorem Ipsum used since the 1500s is reproduced</small>
                                </div>
                            </div>
                            <div class="sl-item">
                                <img class="user rounded-circle" src="images/xs/avatar7.jpg" alt="">
                                <div class="sl-content">
                                    <h5 class="m-b-0">New Email</h5>
                                    <small>45min ago 
                                        <a href="javascript:void(0);" class="text-info">Fidel Tonn</a>.
                                    </small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <h6>Recent Attachments</h6>
                        <ul class="list-unstyled activity">
                            <li>
                                <a href="javascript:void(0)">
                                    <i class="zmdi zmdi-collection-pdf l-blush"></i>
                                    <div class="info">
                                        <h4>info_258.pdf</h4>
                                        <small>2MB</small>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <i class="zmdi zmdi-collection-text l-amber"></i>
                                    <div class="info">
                                        <h4>newdoc_214.doc</h4>
                                        <small>900KB</small>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <i class="zmdi zmdi-image l-parpl"></i>
                                    <div class="info">
                                        <h4>MG_4145.jpg</h4>
                                        <small>5.6MB</small>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <i class="zmdi zmdi-image l-parpl"></i>
                                    <div class="info">
                                        <h4>MG_4100.jpg</h4>
                                        <small>5MB</small>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <i class="zmdi zmdi-collection-text l-amber"></i>
                                    <div class="info">
                                        <h4>Reports_end.doc</h4>
                                        <small>780KB</small>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <i class="zmdi zmdi-videocam l-turquoise"></i>
                                    <div class="info">
                                        <h4>movie2018.MKV</h4>
                                        <small>750MB</small>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </aside>
    <!-- Chat-launcher -->
    <div class="chat-launcher"></div>
    <div class="chat-wrapper">
        <div class="card">
            <div class="header">
                <ul class="list-unstyled team-info margin-0">
                    <li class="m-r-15">
                        <h2>Dr. Team</h2>
                    </li>
                    <li>
                        <img src="images/xs/avatar2.jpg" alt="Avatar">
                    </li>
                    <li>
                        <img src="images/xs/avatar3.jpg" alt="Avatar">
                    </li>
                    <li>
                        <img src="images/xs/avatar4.jpg" alt="Avatar">
                    </li>
                    <li>
                        <img src="images/xs/avatar6.jpg" alt="Avatar">
                    </li>
                    <li>
                        <a href="javascript:void(0);" title="Add Member">
                            <i class="zmdi zmdi-plus-circle"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="chat-widget">
                    <ul class="chat-scroll-list clearfix">
                        <li class="left float-left">
                            <img src="images/xs/avatar3.jpg" class="rounded-circle" alt="">
                            <div class="chat-info">
                                <a class="name" href="#">Alexander</a>
                                <span class="datetime">6:12</span>
                                <span class="message">Hello, John </span>
                            </div>
                        </li>
                        <li class="right">
                            <div class="chat-info">
                                <span class="datetime">6:15</span>
                                <span class="message">Hi, Alexander
                                    <br> How are you!
                                </span>
                            </div>
                        </li>
                        <li class="right">
                            <div class="chat-info">
                                <span class="datetime">6:16</span>
                                <span class="message">There are many variations of passages of Lorem Ipsum available</span>
                            </div>
                        </li>
                        <li class="left float-left">
                            <img src="images/xs/avatar2.jpg" class="rounded-circle" alt="">
                            <div class="chat-info">
                                <a class="name" href="#">Elizabeth</a>
                                <span class="datetime">6:25</span>
                                <span class="message">Hi, Alexander,
                                    <br> John 
                                    <br> What are you doing?
                                </span>
                            </div>
                        </li>
                        <li class="left float-left">
                            <img src="images/xs/avatar1.jpg" class="rounded-circle" alt="">
                            <div class="chat-info">
                                <a class="name" href="#">Michael</a>
                                <span class="datetime">6:28</span>
                                <span class="message">I would love to join the team.</span>
                            </div>
                        </li>
                        <li class="right">
                            <div class="chat-info">
                                <span class="datetime">7:02</span>
                                <span class="message">Hello, 
                                    <br>Michael
                                </span>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="input-group p-t-15">
                    <input type="text" class="form-control" placeholder="Enter text here...">
                    <span class="input-group-addon">
                        <i class="zmdi zmdi-mail-send"></i>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <!-- Main Content -->