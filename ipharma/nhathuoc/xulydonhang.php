<?php include './header.php'; ?>
<!-- Main Content -->
<section class="content home">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12">
                <h2>Quản lý đơn hàng > Xử lý đơn hàng
                    <small>Welcome to iPhama</small>
                </h2>
            </div>            
            <div class="col-lg-7 col-md-7 col-sm-12 text-right">
                <div class="inlineblock text-center m-r-15 m-l-15 hidden-md-down">

                </div>
                <div class="inlineblock text-center m-r-15 m-l-15 hidden-md-down">

                </div>
                <button class="btn btn-white btn-icon btn-round hidden-sm-down float-right m-l-10" type="button">
                    <i class="zmdi zmdi-plus"></i>
                </button>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-home"></i> iPharma</a></li>
                    <li class="breadcrumb-item active">Xử lý đơn hàng</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h4>Thông tin tài khoản</h4>
                    </div>
                    <div class="body">
                        <div class="row">
                            <div class="col-3 col-sm-3 col-md-3 col-lg-3">
                                <p>Tên tài khoản</p>
                            </div>
                            <div class="col-9 col-sm-9 col-md-9 col-lg-9">
                                <p style="color: red;">Nguyễn Văn A</p>
                            </div>
                        </div>
                        <div class="row" style="color: green;">
                            <div class="col-3 col-sm-3 col-md-3 col-lg-3">
                                <p>Địa chỉ</p>
                            </div>
                            <div class="col-9 col-sm-9 col-md-9 col-lg-9">
                                <p>Hà Nội</p>
                            </div>
                        </div>
                        <div class="row" style="color: green;">
                            <div class="col-3 col-sm-3 col-md-3 col-lg-3">
                                <p>Điện thoại</p>
                            </div>
                            <div class="col-9 col-sm-9 col-md-9 col-lg-9">
                                <p>093534545</p>
                            </div>
                        </div>
                        <div class="row" style="color: green;">
                            <div class="col-3 col-sm-3 col-md-3 col-lg-3">
                                <p>Công ty</p>
                            </div>
                            <div class="col-9 col-sm-9 col-md-9 col-lg-9">
                                <p>Nhà thuốc Đông Á</p>
                            </div>
                        </div>
                        <div class="row" style="color: green;">
                            <div class="col-3 col-sm-3 col-md-3 col-lg-3">
                                <p>Quốc gia</p>
                            </div>
                            <div class="col-9 col-sm-9 col-md-9 col-lg-9">
                                <p>Việt Nam</p>
                            </div>
                        </div>
                        <div class="row" style="color: green;">
                            <div class="col-3 col-sm-3 col-md-3 col-lg-3">
                                <p>Zipcode</p>
                            </div>
                            <div class="col-9 col-sm-9 col-md-9 col-lg-9">
                                <p>100000</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="row">
                        <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                            <div class="header">
                                <h4>ĐỊA CHỈ THANH TOÁN</h4>

                            </div>
                            <div class="body">
                                <div class="row" style="color: red;font-size: 18px;font-weight: 500;">
                                    <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                                        <p>Trạng thái thanh toán</p>
                                    </div>
                                    <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                                        <p>Đơn hàng đã hủy</p>
                                    </div>
                                </div>
                                <div class="row" style="color: green;">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                        <p>Luong</p>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                        <p>Hà Nội, Hà Nội, Việt Nam</p>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                        <p>396119479</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                            <div class="header">
                                <h4>ĐỊA CHỈ GIAO HÀNG</h4>

                            </div>
                            <div class="body">
                                <div class="row" style="color: red;font-size: 18px;font-weight: 500;">
                                    <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                                        <p>Trạng thái vận chuyển</p>
                                    </div>
                                    <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                                        <p>Đơn hàng đã hủy</p>
                                    </div>
                                </div>
                                <div class="row" style="color: green;">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                        <p>Luong</p>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                        <p>Hà Nội, Hà Nội, Việt Nam</p>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                        <p>396119479</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">

                    </div>
                    <div class="body">
                        <form>
                            <div class="row">
                                <div class="form-group row" style="width: 100%;">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-2">
                                        <label style="margin-top: 5px;padding-left: 30px;">Mã giảm giá</label>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-9">
                                        <input type="text" class="form-control" id="inputPassword" placeholder="Mã giảm giá">
                                    </div>
                                </div>
                            </div>
                        </form>

                        <div class="tab-content m-t-10">
                            <div class="tab-pane table-responsive active" id="All">

                                <table class="table table-hover">
                                    <thead>
                                        <tr>                                       
                                            <th>Mã thuốc</th>
                                            <th>Tên thuốc</th>
                                            <th>Số lượng</th>
                                            <th>Đơn vị</th>
                                            <th>Đơn giá</th>

                                        </tr>
                                    </thead>
                                    <tbody>

                                        <tr>
                                            <td>1</td>
                                            <td>thuoc cum 1</td>
                                            <td>
                                                10
                                            </td>
                                            <td> vi </td>
                                            <td>1.750.000 đ</td>

                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>thuoc cum 1</td>
                                            <td>
                                                10
                                            </td>
                                            <td> vi </td>
                                            <td>1.750.000 đ</td>

                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>thuoc cum 1</td>
                                            <td>
                                                10
                                            </td>
                                            <td> vi </td>
                                            <td>1.750.000 đ</td>

                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>thuoc cum 1</td>
                                            <td>
                                                10
                                            </td>
                                            <td> vi </td>
                                            <td>1.750.000 đ</td>

                                        </tr>





                                    </tbody>
                                </table>  


                                <div class="row">
                                    <div class="col-6 col-sm-6 col-md-6 col-lg-6">

                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                                        <div class="row" style="margin-left: 30px;">
                                            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="row">
                                                    <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                                                        <p>Tạm tính :</p>
                                                    </div>
                                                    <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                                                        <p>1.750.000 đ</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="row">
                                                    <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                                                        <p>Phí vận chuyển</p>
                                                    </div>
                                                    <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                                                        <p>0 đ</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="row">
                                                    <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                                                        <p>Tổng tiền :</p>
                                                    </div>
                                                    <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                                                        <p>1750000 đ</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>

                                <div class="row">
                                    <div class="col-4 col-sm-4 col-md-4 col-lg-4">

                                    </div>
                                    <div class="col-4 col-sm-4 col-md-4 col-lg-4 text-center">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Xác nhận</button>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

</div>


</section>

<!-- Central Modal Small -->

<!-- Central Modal Small -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Thông báo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Xác nhận thành công
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>

<!-- Jquery Core Js --> 


<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Bootstrap JS and jQuery v3.2.1 -->
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- slimscroll, waves Scripts Plugin Js -->  

<script src="plugins/dropzone/dropzone.js"></script> <!-- Dropzone Plugin Js -->
<script src="plugins/momentjs/moment.js"></script> <!-- Moment Plugin Js -->
<script src="plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

<script src="assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js -->
<script>
    $(function () {
        //Datetimepicker plugin
        $('.datetimepicker').bootstrapMaterialDatePicker({
            format: 'dddd DD MMMM YYYY - HH:mm',
            clearButton: true,
            weekStart: 1
        });
    });
</script>
</body>

<!-- Mirrored from thememakker.com/templates/oreo/hospital/html/light/index.php by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Apr 2019 15:00:40 GMT -->
</html>