<?php include './header.php'; ?>
<!-- Main Content -->
<section class="content home">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12">
                <h2>Báo cáo tài chính
                <small>Welcome to iPhama</small>
                </h2>
            </div>            
             <div class="col-lg-7 col-md-7 col-sm-12 text-right">
                <div class="inlineblock text-center m-r-15 m-l-15 hidden-md-down">
                    
                </div>
                <div class="inlineblock text-center m-r-15 m-l-15 hidden-md-down">
                   
                </div>
                <button class="btn btn-white btn-icon btn-round hidden-sm-down float-right m-l-10" type="button">
                    <i class="zmdi zmdi-plus"></i>
                </button>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-home"></i> iPharma</a></li>
                    <li class="breadcrumb-item active">Báo cáo tài chính</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
         <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                       
                        <ul class="header-dropdown">
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        
                        <form>
          <div class="row">
              <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                                <div class="row">
                                          <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                                              <div class="form-group row">
                                                <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-4 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Tìm theo ngày</label>
                                                <div class="col-12 col-sm-12 col-md-12 col-lg-8" style="padding-left: 0px;padding-right: 0px;">
                                                   <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <i class="zmdi zmdi-calendar"></i>
                                                                </span>
                                                                <input type="text" class="datetimepicker form-control" placeholder="Băt đầu">
                                                            </div>   
                                                </div>
                                              </div>
                                          </div>
                                          <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                                              <div class="form-group row">
                                                <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 18px;padding-right: 0px;">To</label>
                                                <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;">
                                                   <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <i class="zmdi zmdi-calendar"></i>
                                                                </span>
                                                                <input type="text" class="datetimepicker form-control" placeholder="Kết thúc">
                                                    </div>   
                                                </div>
                                              </div>
                                          </div>
                                    </div>
                 <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Ngày tháng ghi số</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;">
                       <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="zmdi zmdi-calendar"></i>
                                    </span>
                                    <input type="text" class="datetimepicker form-control" placeholder="Ngày tháng ghi số">
                                </div>   
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Số hiệu chứng từ</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Số hiệu chứng từ">
                    </div>
                  </div>
                 <div class="form-group row">
                                                <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Ngày chứng từ</label>
                                                <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;">
                                                   <div class="input-group" >
                                                                <span class="input-group-addon">
                                                                    <i class="zmdi zmdi-calendar"></i>
                                                                </span>
                                                                <input type="text" class="datetimepicker form-control" placeholder="Ngày chứng từ">
                                                    </div>   
                                                </div>
                                              </div>
                  
                  
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Tài khoản đối ứng</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Tài khoản đối ứng">
                    </div>
                  </div>
                  
                  
                  
                  
              </div>
              <div class="col- col-sm-6 col-md-6 col-lg-6">
                   <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Tài khoản nợ</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Tài khoản nợ">
                    </div>
                  </div>
                 <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Tài khoản có</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Tài khoản có">
                    </div>
                  </div>
                 <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Số phát sinh nợ</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Số phát sinh nợ">
                    </div>
                  </div>
                 <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Số phát sinh có</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Số phát sinh cós">
                    </div>
                  </div>
                    
              </div>
          </div>
        </form>
                        <div class="row clearfix">
                            <div class="col-md-4">
                                <button type="button" class="btn btn-primary" data-whatever="@mdo">Tìm kiếm</button>
                            </div>
                            <div class="col-md-3">
                                
                            </div>
                            <div class="col-md-5">
                                <button type="button" class="btn btn-primary" data-whatever="@mdo">Xuất báo cáo</button>
                            </div>

                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                         <ul class="header-dropdown">
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body" style="padding-left: 0px;padding-right: 0px;">
                        <div class="tab-content m-t-10">
                            <div class="tab-pane table-responsive active" id="All">
                                <h6 style="padding-left: 10px;">Năm 2019</h6>
                                <h6 style="padding-left: 10px;">Đơn vị tính : VNĐ</h6>
                                <table class="table table-hover">
                                    <thead>
                                        <tr>                                       
                                            <th rowspan="2" style="border: 2px solid #dee2e6;vertical-align: middle;">Ngày tháng ghi sổ</th>
                                            <th colspan="2" style="border: 2px solid #dee2e6;vertical-align: middle;">Chứng từ</th>
                                            <th rowspan="2" style="border: 2px solid #dee2e6;vertical-align: middle;">Diễn giải</th>
                                            <th rowspan="2" style="border: 2px solid #dee2e6;vertical-align: middle;">TK nợ TK có</th>
                                            <th rowspan="2" style="border: 2px solid #dee2e6;vertical-align: middle;">TK đối tượng</th>
                                            <th colspan="2" style="border: 2px solid #dee2e6;vertical-align: middle;">Số phát sinh</th>
                                            
                                        </tr>
                                        <tr>                                 
                                            
                                            <th style="border: 2px solid #dee2e6;vertical-align: middle;">Số hiệu</th>
                                            <th>Ngày</th>
                                            
                                            
                                            
                                            <th style="border: 2px solid #dee2e6;vertical-align: middle;">Nợ</th>
                                            <th style="border: 2px solid #dee2e6;vertical-align: middle;">Có</th>
                                            
                                            
                                        </tr>
                                        <tr>                                 
                                            <th style="border: 2px solid #dee2e6;vertical-align: middle;">A</th>
                                            <th style="border: 2px solid #dee2e6;vertical-align: middle;">1</th>
                                            <th style="border: 2px solid #dee2e6;vertical-align: middle;">C</th>
                                            <th style="border: 2px solid #dee2e6;vertical-align: middle;">D</th>
                                            <td style="border: 2px solid #dee2e6;vertical-align: middle;"></td>
                                            <th style="border: 2px solid #dee2e6;vertical-align: middle;" >F</th>
                                            <th style="border: 2px solid #dee2e6;vertical-align: middle;">(1)</th>
                                            <th style="border: 2px solid #dee2e6;vertical-align: middle;">(2)</th>
                                           
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                       
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td colspan="3" style="border: 2px solid #dee2e6;vertical-align: middle;">Phát sinh trong kỳ</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>1/1/2019</td>
                                            <td>PKT001</td>
                                            <td>1/1/2019</td>
                                            <td>Kết chuyển lãi năm 2016</td>
                                            <td>4212</td>
                                            <td>4211</td>
                                            <td>75.365.365</td>
                                            <td></td>
                                        </tr>
                                         <tr>
                                            <td>1/1/2019</td>
                                            <td>PKT001</td>
                                            <td>1/1/2019</td>
                                            <td>Kết chuyển lãi năm 2016</td>
                                            <td>4212</td>
                                            <td>4211</td>
                                            <td></td>
                                            <td>75.365.365</td>
                                        </tr>
                                        
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <th>Tổng cộng</th>
                                            <td></td>
                                            <td></td>
                                            <td>75.365.365</td>
                                            <td>75.365.365</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <th>Tháng 2</th>
                                            <td></td>
                                            <td></td>
                                            <td>75.365.365</td>
                                            <td>75.365.365</td>
                                            <td></td>
                                        </tr>
                                         
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <th colspan="3">Cộng phát sinh năm 2017</th>
                                            <th>75.365.365</th>
                                            <th>75.365.365</th>
                                           
                                        </tr>

                                        
                                       
                                      
                                        
                                         
                                        
                                    </tbody>
                                </table>   

                            </div>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
                  
    </div>


</section>

<!-- Central Modal Small -->
<div class="modal fade" id="centralModalSm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Change class .modal-sm to change the size of the modal -->
  <div class="modal-dialog modal-lg" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Thêm mới</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form>
          <div class="row">
              <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                 <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Khách hàng ID</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="password" class="form-control" id="inputPassword" placeholder="Khách hàng ID">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Tên</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="password" class="form-control" id="inputPassword" placeholder="Tên">
                    </div>
                  </div>
                 <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Tuổi</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="password" class="form-control" id="inputPassword" placeholder="Tuổi">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Địa chỉ</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="password" class="form-control" id="inputPassword" placeholder="Địa chỉ">
                    </div>
                  </div>
                  
                  
                  
              </div>
              <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                    
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 15px;padding-right: 0px;">Giớ tính</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="password" class="form-control" id="inputPassword" placeholder="Giới tính">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Email</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                       <input type="text" class="form-control" id="datepicker" placeholder="Email">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Số điện thoại</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                       <input type="text" class="form-control" id="datepicker" placeholder="Số điện thoại">
                    </div>
                  </div>
                   <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-3 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Ngày đăng kí</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                       <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="zmdi zmdi-calendar"></i>
                                    </span>
                                    <input type="text" class="datetimepicker form-control" placeholder="Ngày đăng kí">
                                </div>   
                    </div>
                  </div>
              </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        
        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal">Thêm mới</button>
      </div>
    </div>
  </div>
</div>
<!-- Central Modal Small -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Thông báo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Thêm mới thành công
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>

<!-- Jquery Core Js --> 


<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Bootstrap JS and jQuery v3.2.1 -->
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- slimscroll, waves Scripts Plugin Js -->  

<script src="plugins/dropzone/dropzone.js"></script> <!-- Dropzone Plugin Js -->
<script src="plugins/momentjs/moment.js"></script> <!-- Moment Plugin Js -->
<script src="plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

<script src="assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js -->
<script>
    $(function () {
    //Datetimepicker plugin
    $('.datetimepicker').bootstrapMaterialDatePicker({
        format: 'dddd DD MMMM YYYY - HH:mm',
        clearButton: true,
        weekStart: 1
    });
});
</script>
</body>

<!-- Mirrored from thememakker.com/templates/oreo/hospital/html/light/index.php by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Apr 2019 15:00:40 GMT -->
</html>