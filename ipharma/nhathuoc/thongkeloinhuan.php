<?php include './header.php'; ?>
<!-- Main Content -->
<section class="content home">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12">
                <h2>Thống kê lợi nhuận
                <small>Welcome to iPhama</small>
                </h2>
            </div>            
             <div class="col-lg-7 col-md-7 col-sm-12 text-right">
                <div class="inlineblock text-center m-r-15 m-l-15 hidden-md-down">
                    
                </div>
                <div class="inlineblock text-center m-r-15 m-l-15 hidden-md-down">
                   
                </div>
                <button class="btn btn-white btn-icon btn-round hidden-sm-down float-right m-l-10" type="button">
                    <i class="zmdi zmdi-plus"></i>
                </button>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-home"></i> iPharma</a></li>
                    <li class="breadcrumb-item active">Thống kê lợi nhuận</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
         <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                       
                        <ul class="header-dropdown">
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        
                        <form>
          <div class="row">
              <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                <div class="form-group row">
                        <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Quý</label>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;margin-top: -10px;">
                         <select class="">
                                    <option value="">Quý 1</option>
                                    <option value="10">Quý 2</option>
                                    <option value="20">Quý 3</option>
                                    <option value="20">Quý 4</option>
                        </select>
                        </div>
                  </div>
                 <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Ngày</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;">
                       <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="zmdi zmdi-calendar"></i>
                                    </span>
                                    <input type="text" class="datetimepicker form-control" placeholder="Ngày">
                                </div>   
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Mã đơn  hàng</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Mã đơn hàng">
                    </div>
                  </div>
                 
                  
                  
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Mã hóa đơn</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Má hóa đơn">
                    </div>
                  </div>
                   <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Nhân viên bán hàng</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Nhân viên bán hàng">
                    </div>
                  </div>
                  
                  
                  
              </div>
              <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                   <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Đơn giá</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Đơn giá">
                    </div>
                  </div>
                 <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Phí vận chuyển</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Phí vận chuyển">
                    </div>
                  </div>
                 <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Đã thanh toán</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Đã thanh toán">
                    </div>
                  </div>
                 <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Tiền nợ</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Tiền nợ">
                    </div>
                  </div>
                    
              </div>
          </div>
        </form>
                        <div class="row clearfix">
                            <div class="col-md-4">
                                <button type="button" class="btn btn-primary" data-whatever="@mdo">Tìm kiếm</button>
                            </div>
                            <div class="col-md-3">
                                
                            </div>
                            <div class="col-md-5">
                                <button type="button" class="btn btn-primary" data-whatever="@mdo">Xuất báo cáo</button>
                            </div>

                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                         <ul class="header-dropdown">
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="tab-content m-t-10">
                            <div class="tab-pane table-responsive active" id="All">
                                
                                <table class="table table-hover">
                                    <thead>
                                        <tr>                                       
                                            <th>STT</th>
                                            <th>Quý</th>
                                            <th>Ngày</th>
                                            <th>Mã đơn hàng</th>
                                            <th>Mã hóa đơn</th>
                                            <th>Nhân viên bán hàng</th>
                                            <th>Đơn giá</th>
                                            <th>Phí vận chuyển</th>
                                            <th>Đã thanh toán</th>
                                            <th>Tiền nợ</th>
                                            <th>Lợi nhuận</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>                                 
                                            <td>1</td>
                                            <td>1</td>
                                            <td>1/1/2019</td>
                                            <td>1</td>
                                            <td>1</td>
                                            <td>Nguyễn Văn A</td>
                                            <td>100.000 đ</td>
                                            <td>15.000 đ</td>
                                            <td>90.000 đ</td>
                                            <td>25.000 đ</td>
                                            <td>15.000 đ</td>
                                            
                                        </tr>
                                        <tr>                                 
                                            <td>1</td>
                                            <td>1</td>
                                            <td>1/1/2019</td>
                                            <td>1</td>
                                            <td>1</td>
                                            <td>Nguyễn Văn A</td>
                                            <td>100.000 đ</td>
                                            <td>15.000 đ</td>
                                            <td>90.000 đ</td>
                                            <td>25.000 đ</td>
                                            <td>15.000 đ</td>
                                            
                                        </tr>
                                        <tr>                                 
                                            <td>1</td>
                                            <td>1</td>
                                            <td>1/1/2019</td>
                                            <td>1</td>
                                            <td>1</td>
                                            <td>Nguyễn Văn A</td>
                                            <td>100.000 đ</td>
                                            <td>15.000 đ</td>
                                            <td>90.000 đ</td>
                                            <td>25.000 đ</td>
                                            <td>15.000 đ</td>
                                            
                                        </tr>
                                        <tr>                                 
                                            <td>1</td>
                                            <td>1</td>
                                            <td>1/1/2019</td>
                                            <td>1</td>
                                            <td>1</td>
                                            <td>Nguyễn Văn A</td>
                                            <td>100.000 đ</td>
                                            <td>15.000 đ</td>
                                            <td>90.000 đ</td>
                                            <td>25.000 đ</td>
                                            <td>15.000 đ</td>
                                            
                                        </tr>
                                        <tr>                                 
                                            <td>1</td>
                                            <td>1</td>
                                            <td>1/1/2019</td>
                                            <td>1</td>
                                            <td>1</td>
                                            <td>Nguyễn Văn A</td>
                                            <td>100.000 đ</td>
                                            <td>15.000 đ</td>
                                            <td>90.000 đ</td>
                                            <td>25.000 đ</td>
                                            <td>15.000 đ</td>
                                            
                                        </tr>
                                        
                                         
                                        
                                    </tbody>
                                </table>   

                            </div>
                           <div class="row" style="margin-top: 50px;">
                            <div class="col-4 col-sm-4 col-md-4 col-lg-4">
                                
                            </div>
                            <div class="col-8 col-sm-8 col-md-8 col-lg-8">
                                <div class="row">
                                    <div class="col-3 col-sm-3 col-md-3 col-lg-3">
                                        <p>Tổng doanh thu : </p>
                                    </div>
                                    <div class="col-3 col-sm-3 col-md-3 col-lg-3">
                                        <p style="color: red;">15.000.000</p>
                                    </div>
                                    <div class="col-3 col-sm-3 col-md-3 col-lg-3">
                                        <p>Tổng lợi nhuận : </p>
                                    </div>
                                    <div class="col-3 col-sm-3 col-md-3 col-lg-3">
                                        <p style="color: red;">5.000.000</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                  
    </div>


</section>

<!-- Central Modal Small -->
<div class="modal fade" id="centralModalSm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Change class .modal-sm to change the size of the modal -->
  <div class="modal-dialog modal-lg" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Thêm mới</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form>
          <div class="row">
              <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                <div class="form-group row">
                        <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 15px;padding-right: 0px;">Quý</label>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: -1px;margin-top: -10px;">
                         <select class="form-control show-tick" style="width: 97%;">
                                    <option value="">Quý 1</option>
                                    <option value="10">Quý 2</option>
                                    <option value="20">Quý 3</option>
                                    <option value="20">Quý 4</option>
                        </select>
                        </div>
                  </div>
                 <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Ngày</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                       <div class="input-group" style="margin-left: 14px;">
                                    <span class="input-group-addon">
                                        <i class="zmdi zmdi-calendar"></i>
                                    </span>
                                    <input type="text" class="datetimepicker form-control" placeholder="Ngày">
                                </div>   
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Má đơn  hàng</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Mã đơn hàng" style="margin-left: 15px;">
                    </div>
                  </div>
                 
                  
                  
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Mã hóa đơn</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Má hóa đơn" style="margin-left: 15px">
                    </div>
                  </div>
                   <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Nhân viên bán hàng</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Nhân viên bán hàng" style="margin-left: 15px;">
                    </div>
                  </div>
                  
                  
                  
              </div>
              <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                   <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Đơn giá</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Đơn giá" style="margin-left: 15px;">
                    </div>
                  </div>
                 <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Phí vận chuyển</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Phí vận chuyển" style="margin-left: 15px;">
                    </div>
                  </div>
                 <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Đã thanh toán</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Đã thanh toán" style="margin-left: 15px;">
                    </div>
                  </div>
                 <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Tiền nợ</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Tiền nợ" style="margin-left: 15px;">
                    </div>
                  </div>
                    
              </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        
        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal">Thêm mới</button>
      </div>
    </div>
  </div>
</div>
<!-- Central Modal Small -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Thông báo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Thêm mới thành công
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>

<!-- Jquery Core Js --> 


<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Bootstrap JS and jQuery v3.2.1 -->
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- slimscroll, waves Scripts Plugin Js -->  

<script src="plugins/dropzone/dropzone.js"></script> <!-- Dropzone Plugin Js -->
<script src="plugins/momentjs/moment.js"></script> <!-- Moment Plugin Js -->
<script src="plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

<script src="assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js -->
<script>
    $(function () {
    //Datetimepicker plugin
    $('.datetimepicker').bootstrapMaterialDatePicker({
        format: 'dddd DD MMMM YYYY - HH:mm',
        clearButton: true,
        weekStart: 1
    });
});
</script>
</body>

<!-- Mirrored from thememakker.com/templates/oreo/hospital/html/light/index.php by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Apr 2019 15:00:40 GMT -->
</html>