<?php include './header.php'; ?>
<!-- Main Content -->
<section class="content home">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12">
                <h2>Quản lý người dùng ứng dụng > Quản lý phân quyền theo role
                <small>Welcome to iPhama</small>
                </h2>
            </div>            
             <div class="col-lg-7 col-md-7 col-sm-12 text-right">
                <div class="inlineblock text-center m-r-15 m-l-15 hidden-md-down">
                    
                </div>
                <div class="inlineblock text-center m-r-15 m-l-15 hidden-md-down">
                   
                </div>
                <button class="btn btn-white btn-icon btn-round hidden-sm-down float-right m-l-10" type="button">
                    <i class="zmdi zmdi-plus"></i>
                </button>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-home"></i> iPharma</a></li>
                    <li class="breadcrumb-item active">Quản lý phân quyền theo role</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
         <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                       
                        <ul class="header-dropdown">
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <form>
          <div class="row">
              <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                  
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Tên role</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Tên role">
                    </div>
                  </div>
                   <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Mô tả</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Mô tả">
                    </div>
                  </div>
                  
                  
                  
              </div>
              <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                    <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Người tạo</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Người tạo" >
                    </div>
                  </div>
                   <div class="form-group row">
                                                <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Ngày tạo</label>
                                                <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;">
                                                   <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <i class="zmdi zmdi-calendar"></i>
                                                                </span>
                                                                <input type="text" class="datetimepicker form-control" placeholder="Ngày tạo">
                                                            </div>   
                                                </div>
                                              </div>
                 
                  
                  
                 
              </div>
          </div>
        </form>
                        <div class="row clearfix">
                            <div class="col-3 col-md-3">
                                
                            </div>
                            <div class="col-3 col-md-3">
                                <button type="button" class="btn btn-primary"  data-whatever="@mdo">Tìm kiếm</button>
                            </div>
                            <div class="col-md-3">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#centralModalSm" data-whatever="@mdo">Thêm mới</button>
                            </div>

                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                         <ul class="header-dropdown">
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="tab-content m-t-10">
                            <div class="tab-pane table-responsive active" id="All">
                                <h5>Bảng chức năng phân quyền cho Role : Quản trị viên</h5>
                                <table class="table table-hover">
                                    <thead>
                                        <tr>                                       
                                            <th>Danh mục quyền</th>
                                            <th>Xem</th>
                                           
                                            <th>Sửa</th>
                                            <th>Xóa</th>
                                            <th>Import</th>
                                            <th>Export</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>                                 
                                            <td>Quản lý đối tác (nhà thuốc)</td>
                                            <td>
                                              <div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
  <label class="form-check-label" for="inlineCheckbox1"></label>
</div>
                                            </td>
                                            <td> <div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
  <label class="form-check-label" for="inlineCheckbox1"></label>
</div></td>
                                            <td> <div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
  <label class="form-check-label" for="inlineCheckbox1"></label>
</div></td>
                                            <td></td>
                                            <td></td>
                                           
                                        </tr>
                                         <tr>                                 
                                            <td>Quản lý đơn hàng</td>
                                            <td> <div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
  <label class="form-check-label" for="inlineCheckbox1"></label>
</div></td>
                                            <td> <div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
  <label class="form-check-label" for="inlineCheckbox1"></label>
</div></td>
                                            <td> <div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
  <label class="form-check-label" for="inlineCheckbox1"></label>
</div></td>
                                            <td></td>
                                            <td></td>
                                           
                                        </tr>
                                         <tr>                                 
                                            <td>Quản lý thư viện thuốc</td>
                                            <td> <div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
  <label class="form-check-label" for="inlineCheckbox1"></label>
</div></td>
                                            <td> <div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
  <label class="form-check-label" for="inlineCheckbox1"></label>
</div></td>
                                            <td> <div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
  <label class="form-check-label" for="inlineCheckbox1"></label>
</div></td>
                                            <td></td>
                                            <td></td>
                                           
                                        </tr>
                                         <tr>                                 
                                            <td>Quản lý người dùng ứng</td>
                                            <td> <div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
  <label class="form-check-label" for="inlineCheckbox1"></label>
</div></td>
                                            <td> <div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
  <label class="form-check-label" for="inlineCheckbox1"></label>
</div></td>
                                            <td> <div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
  <label class="form-check-label" for="inlineCheckbox1"></label>
</div></td>
                                            <td></td>
                                            <td></td>
                                           
                                        </tr>
                                         <tr>                                 
                                            <td>Cấu hình</td>
                                            <td> <div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
  <label class="form-check-label" for="inlineCheckbox1"></label>
</div></td>
                                            <td> <div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
  <label class="form-check-label" for="inlineCheckbox1"></label>
</div></td>
                                            <td> <div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
  <label class="form-check-label" for="inlineCheckbox1"></label>
</div></td>
                                            <td></td>
                                            <td></td>
                                           
                                        </tr>
                                        
                                    </tbody>
                                </table>                            
                            </div>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
                  
    </div>


</section>

<!-- Central Modal Small -->
<div class="modal fade" id="centralModalSm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Change class .modal-sm to change the size of the modal -->
  <div class="modal-dialog modal-lg" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Thêm mới</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <form>
          <div class="row">
              <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                  
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Tên role</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Tên role">
                    </div>
                  </div>
                   <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Mô tả</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Mô tả">
                    </div>
                  </div>
                  
                  
                  
              </div>
              <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                    <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Người tạo</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Người tạo">
                    </div>
                  </div>
                   <div class="form-group row">
                                                <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Ngày tạo</label>
                                                <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;">
                                                   <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <i class="zmdi zmdi-calendar"></i>
                                                                </span>
                                                                <input type="text" class="datetimepicker form-control" placeholder="Ngày tạo">
                                                            </div>   
                                                </div>
                                              </div>
                 
                  
                  
                 
              </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        
        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal">Thêm mới</button>
      </div>
    </div>
  </div>
</div>
<!-- Central Modal Small -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Thông báo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Thêm mới thành công
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>

<!-- Jquery Core Js --> 


<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Bootstrap JS and jQuery v3.2.1 -->
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- slimscroll, waves Scripts Plugin Js -->  

<script src="plugins/dropzone/dropzone.js"></script> <!-- Dropzone Plugin Js -->
<script src="plugins/momentjs/moment.js"></script> <!-- Moment Plugin Js -->
<script src="plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

<script src="assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js -->
<script>
    $(function () {
    //Datetimepicker plugin
    $('.datetimepicker').bootstrapMaterialDatePicker({
        format: 'dddd DD MMMM YYYY - HH:mm',
        clearButton: true,
        weekStart: 1
    });
});
</script>
</body>

<!-- Mirrored from thememakker.com/templates/oreo/hospital/html/light/index.php by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Apr 2019 15:00:40 GMT -->
</html>