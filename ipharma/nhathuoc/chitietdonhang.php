<?php include './header.php'; ?>
<!-- Main Content -->
<section class="content home">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12">
                <h2>Web đại lý > Chi tiết đơn hàng
                <small>Welcome to iPhama</small>
                </h2>
            </div>            
             <div class="col-lg-7 col-md-7 col-sm-12 text-right">
                <div class="inlineblock text-center m-r-15 m-l-15 hidden-md-down">
                    
                </div>
                <div class="inlineblock text-center m-r-15 m-l-15 hidden-md-down">
                   
                </div>
                <button class="btn btn-white btn-icon btn-round hidden-sm-down float-right m-l-10" type="button">
                    <i class="zmdi zmdi-plus"></i>
                </button>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-home"></i> iPharma</a></li>
                    <li class="breadcrumb-item active">Chi tiết đơn hàng</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
         <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                       
                        <ul class="header-dropdown">
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                       
                        <div class="row">
                          <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                            <div class="row">
                              <div class="col-4 col-sm-4 col-md-4 col-lg-4">
                                <p>Nhà thuốc ID</p>
                              </div>
                              <div class="col-8 col-sm-8 col-md-8 col-lg-8">
                                <h6>Nhà thuốc iPharma</h6>
                              </div>
                              
                            </div>
                            <div class="row">
                              <div class="col-4 col-sm-4 col-md-4 col-lg-4">
                                <p>Khách hàng</p>
                              </div>
                              <div class="col-4 col-sm-4 col-md-4 col-lg-4">
                                <p>Tên</p>
                              </div>
                              <div class="col-4 col-sm-4 col-md-4 col-lg-4">
                                <h6>Nhân viên A</h6>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-4 col-sm-4 col-md-4 col-lg-4">
                                
                              </div>
                              <div class="col-4 col-sm-4 col-md-4 col-lg-4">
                                <p>Địa chỉ</p>
                              </div>
                              <div class="col-4 col-sm-4 col-md-4 col-lg-4">
                                <h6>Hà Nội</h6>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-4 col-sm-4 col-md-4 col-lg-4">
                                
                              </div>
                              <div class="col-4 col-sm-4 col-md-4 col-lg-4">
                                <p>Số điện thoại</p>
                              </div>
                              <div class="col-4 col-sm-4 col-md-4 col-lg-4">
                                <h6>234345</h6>
                              </div>
                            </div>
                          </div>
                          <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                            <div class="row">
                              <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                                <p>Phân loại</p>
                              </div>
                              <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                                <h6>Đơn thuốc theo bệnh viện, phòng khám</h6>
                              </div>
                              <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                                <p>Mã đơn thuốc</p>
                              </div>
                              <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                                <h6>DH0005651</h6>
                              </div>
                              <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                                <p>Trạng thái</p>
                              </div>
                              <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                                <h6>Tiếp nhận</h6>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                         <ul class="header-dropdown">
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="tab-content m-t-10">
                            <div class="tab-pane table-responsive active" id="All">
                                <h6>Chi tiết đơn thuốc</h6>
                                <table class="table table-hover">
                                    <thead>
                                        <tr>                                       
                                            <th>Mã thuốc</th>
                                            <th>Tên thuốc</th>
                                           
                                            <th>Số lượng</th>
                                            <th>Đơn vị</th>
                                            <th>Chiết khấu</th>
                                            <th>Báo giá</th>
                                            
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>                                 
                                            <td>DH0005651</td>
                                            <td>Thuốc 1</td>
                                            <td>10</td>
                                            <td>hộp</td>
                                            <td>5%</td>
                                            <td>10.000.000 đ</td>
                                            
                                            
                                        </tr>
                                       <tr>                                 
                                            <td>DH0005651</td>
                                            <td>Thuốc 1</td>
                                            <td>10</td>
                                            <td>hộp</td>
                                            <td>5%</td>
                                            <td>10.000.000 đ</td>
                                            
                                            
                                        </tr>
                                        <tr>                                 
                                            <td>DH0005651</td>
                                            <td>Thuốc 1</td>
                                            <td>10</td>
                                            <td>hộp</td>
                                            <td>5%</td>
                                            <td>10.000.000 đ</td>
                                            
                                            
                                        </tr>
                                        <tr>                                 
                                            <td>DH0005651</td>
                                            <td>Thuốc 1</td>
                                            <td>10</td>
                                            <td>hộp</td>
                                            <td>5%</td>
                                            <td>10.000.000 đ</td>
                                            
                                            
                                        </tr>
                                         
                                        
                                    </tbody>
                                </table>     
                                <div class="row">
                                                 <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                                                       
                                                     </div>  
                                                     <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                                                         <table class="table table-hover">
                                  <tr>
                                    <td>Tạm tính</td>
                                    <td>50.000.000 đ</td>
                                  </tr>
                                  <tr>
                                    <td>Phí vận chuyển</td>
                                    <td>1.000.000 đ</td>
                                  </tr>
                                  <tr>
                                    <td>Tổng tiền</td>
                                    <td>45.000.000 đ</td>
                                  </tr>
                                </table>    
                                                       </div>  
                                                   </div>                   
                            </div>
                           
                        </div>

                        <div class="row" style="margin-top: 25px;">
                          <div class="col-3 col-sm-3 col-md-3 col-lg-3">
                            
                          </div>
                          <div class="col-3 col-sm-3 col-md-3 col-lg-3">
                              <a href="xulydonhang.php" class="btn btn-primary" title="">Tiếp nhận</a>
                             <!--<button type="button" class="btn btn-primary"  data-whatever="@mdo">Tiếp nhận</button>-->
                          </div>
                          <div class="col-3 col-sm-3 col-md-3 col-lg-3">
                             <button type="button" class="btn btn-primary"  data-whatever="@mdo">Từ chối</button>
                          </div>
                          <div class="col-3 col-sm-3 col-md-3 col-lg-3">
                            
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                  
    </div>


</section>

<!-- Central Modal Small -->
<div class="modal fade" id="centralModalSm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Change class .modal-sm to change the size of the modal -->
  <div class="modal-dialog modal-lg" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Thêm mới</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="row">
              <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                 
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Nhà thuốc ID</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Nhà thuốc ID">
                    </div>
                  </div>
                  <div class="form-group row" style="margin-top: 25px;">
                        <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Thuộc đại lý</label>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;margin-top: -5px;">
                         <select style="width: 104%;" class="">
                                    <option value="">Đại lý 1</option>
                                    <option value="10">Đại lý 2</option>
                                    <option value="20">Đại lý 3</option>
                                    <option value="20">Đại lý 4</option>
                                    <option value="20">Đại lý 5</option>
                        </select>
                        </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 10px;">Người đại diện</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Người đại diện">
                    </div>
                  </div>
                   <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 10px;">Địa chỉ</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Địa chỉ">
                    </div>
                  </div>
                   <div class="form-group row" style="margin-top: 20px;">
                    <label for="text" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Hoạt động từ</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;">
                       <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="zmdi zmdi-calendar"></i>
                                    </span>
                                    <input type="text" class="datetimepicker form-control" placeholder="Hoạt động tử">
                                </div>   
                    </div>
                  </div>
                 
                  
                  
                  
              </div>
              <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                    
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Loại nhà thuốc</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Loại nhà thuốc">
                    </div>
                  </div>
                  <div class="form-group row" style="margin-top: 20px;">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Số điện thoại</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;">
                       <input type="text" class="form-control" id="datepicker" placeholder="Số điện thoại">
                    </div>
                  </div>
                  <div class="form-group row" style="margin-top: 20px;">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Mã số kinh doanh</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;">
                       <input type="text" class="form-control" id="datepicker" placeholder="Mã số kinh doanh">
                    </div>
                  </div>
                   <div class="form-group row" style="margin-top: 20px;">
                    <label for="text" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Thời gian mở cửa</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-10" style="padding-left: 0px;padding-right: 0px;">
                       <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="zmdi zmdi-calendar"></i>
                                    </span>
                                    <input type="text" class="datetimepicker form-control" placeholder="Thời gian mở cửa">
                                </div>   
                    </div>
                  </div>
              </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        
        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal">Thêm mới</button>
      </div>
    </div>
  </div>
</div>
<!-- Central Modal Small -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Thông báo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Thêm mới thành công
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>

<!-- Jquery Core Js --> 


<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Bootstrap JS and jQuery v3.2.1 -->
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- slimscroll, waves Scripts Plugin Js -->  

<script src="plugins/dropzone/dropzone.js"></script> <!-- Dropzone Plugin Js -->
<script src="plugins/momentjs/moment.js"></script> <!-- Moment Plugin Js -->
<script src="plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

<script src="assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js -->
<script>
    $(function () {
    //Datetimepicker plugin
    $('.datetimepicker').bootstrapMaterialDatePicker({
        format: 'dddd DD MMMM YYYY - HH:mm',
        clearButton: true,
        weekStart: 1
    });
});
</script>
</body>

<!-- Mirrored from thememakker.com/templates/oreo/hospital/html/light/index.php by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Apr 2019 15:00:40 GMT -->
</html>