<?php include './header.php'; ?>
<!-- Main Content -->
<section class="content home">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12">
                <h2>Báo cáo doanh thu > Recommentdation
                <small>Welcome to iPhama</small>
                </h2>
            </div>            
             <div class="col-lg-7 col-md-7 col-sm-12 text-right">
                <div class="inlineblock text-center m-r-15 m-l-15 hidden-md-down">
                    
                </div>
                <div class="inlineblock text-center m-r-15 m-l-15 hidden-md-down">
                   
                </div>
                <button class="btn btn-white btn-icon btn-round hidden-sm-down float-right m-l-10" type="button">
                    <i class="zmdi zmdi-plus"></i>
                </button>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-home"></i> iPharma</a></li>
                    <li class="breadcrumb-item active">Recommendation</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
         <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                       
                        <ul class="header-dropdown">
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        
                        <form>
          <div class="row">
              <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                                
                 
                  <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Tên tag</label>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-9" style="padding-left: 0px;padding-right: 0px;">
                      <input type="text" class="form-control" id="inputPassword" placeholder="Ghi tác">
                    </div>
                  </div>
                 
                  
                  
                  
                  
                  
                  
              </div>
              <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                  <div class="form-group row">
                       
                            <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-2 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Kích thước</label>
                       
                       <div class="col-6 col-sm-6 col-md-6 col-lg-5">
                           <input type="text" class="form-control" id="inputPassword" placeholder="Chiều rộng" style="margin-left: 15px">
                       </div>
                       <div class="col-6 col-sm-6 col-md-6 col-lg-5">
                           <input type="text" class="form-control" id="inputPassword" placeholder="Chiều cao" style="margin-left: 15px">
                       </div>
                   </div> 
                 
                 
                
                    
              </div>

              <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                <div class="form-group row">
                    <label for="inputPassword" class="col-12 col-sm-12 col-md-12 col-lg-1 col-form-label" style="font-size: 11px;padding-left: 10px;padding-right: 0px;">Ghi chú</label>
                    
                    <textarea class="form-control col-lg-11" id="exampleFormControlTextarea1" rows="2" style="border: 1px solid #eee;border-radius: 100px"></textarea>
                  
            </div>
              </div>


          </div>
          
        </form>
                        <div class="row clearfix">
                            <div class="col-md-4">
                                <button type="button" class="btn btn-primary" data-whatever="@mdo">Tìm kiếm</button>
                            </div>
                            <div class="col-md-3">
                                
                            </div>
                            <div class="col-md-5">
                                <button type="button" class="btn btn-primary" data-whatever="@mdo">Tạo tag</button>
                            </div>

                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                         <ul class="header-dropdown">
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body" style="padding-left: 0px;padding-right: 0px;">
                        <div class="tab-content m-t-10">
                            <div class="tab-pane table-responsive active" id="All">
                               
                                <table class="table table-hover">
                                    <thead>
                                        <tr>                                       
                                            <th>Mã tag</th>
                                            <th>Tên tag</th>
                                            <th>Chiều rộng</th>
                                            <th>Chiều cao</th>
                                            <th>Ghi chú</th>
                                            <th>Trạng thái</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                        
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>TAG001</td>
                                            <td>quảng cáo 1</td>
                                            <td>150</td>
                                            <td>150</td>
                                            <td>Quảng cáo top</td>
                                            <td>Hoạt động</td>
                                            <td>
                                                <a href="#" title="" data-toggle="modal" data-target="#centralModalSm">Biên tập</a>
                                                <a href="#" title=""><i class="fas fa-pen" style="padding-left: 30px;color: green;"></i></a>
                                                <a href="#" title=""><i class="fas fa-trash" style="padding-left: 30px;color: red;"></i></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>TAG002</td>
                                            <td>quảng cáo 1</td>
                                            <td>150</td>
                                            <td>150</td>
                                            <td>Quảng cáo top</td>
                                            <td>Hoạt động</td>
                                            <td>
                                                <a href="#" title="" data-toggle="modal" data-target="#centralModalSm">Biên tập</a>
                                                <a href="#" title=""><i class="fas fa-pen" style="padding-left: 30px;color: green;"></i></a>
                                                <a href="#" title=""><i class="fas fa-trash" style="padding-left: 30px;color: red;"></i></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>TAG002</td>
                                            <td>quảng cáo 1</td>
                                            <td>150</td>
                                            <td>150</td>
                                            <td>Quảng cáo top</td>
                                            <td>Hoạt động</td>
                                            <td>
                                                <a href="#" title="" data-toggle="modal" data-target="#centralModalSm">Biên tập</a>
                                                <a href="#" title=""><i class="fas fa-pen" style="padding-left: 30px;color: green;"></i></a>
                                                <a href="#" title=""><i class="fas fa-trash" style="padding-left: 30px;color: red;"></i></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>TAG003</td>
                                            <td>quảng cáo 1</td>
                                            <td>150</td>
                                            <td>150</td>
                                            <td>Quảng cáo top</td>
                                            <td>Hoạt động</td>
                                            <td>
                                                <a href="#" title="" data-toggle="modal" data-target="#centralModalSm">Biên tập</a>
                                                <a href="#" title=""><i class="fas fa-pen" style="padding-left: 30px;color: green;"></i></a>
                                                <a href="#" title=""><i class="fas fa-trash" style="padding-left: 30px;color: red;"></i></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>TAG004</td>
                                            <td>quảng cáo 1</td>
                                            <td>150</td>
                                            <td>150</td>
                                            <td>Quảng cáo top</td>
                                            <td>Hoạt động</td>
                                            <td>
                                                <a href="#" title="" data-toggle="modal" data-target="#centralModalSm">Biên tập</a>
                                                <a href="#" title=""><i class="fas fa-pen" style="padding-left: 30px;color: green;"></i></a>
                                                <a href="#" title=""><i class="fas fa-trash" style="padding-left: 30px;color: red;"></i></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>TAG005</td>
                                            <td>quảng cáo 1</td>
                                            <td>150</td>
                                            <td>150</td>
                                            <td>Quảng cáo top</td>
                                            <td>Hoạt động</td>
                                            <td>
                                                <a href="#" title="" data-toggle="modal" data-target="#centralModalSm">Biên tập</a>
                                                <a href="#" title=""><i class="fas fa-pen" style="padding-left: 30px;color: green;"></i></a>
                                                <a href="#" title=""><i class="fas fa-trash" style="padding-left: 30px;color: red;"></i></a>
                                            </td>
                                        </tr>


                                        
                                       
                                      
                                        
                                         
                                        
                                    </tbody>
                                </table>   

                            </div>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
                  
    </div>


</section>

<!-- Central Modal Small -->
<div class="modal fade" id="centralModalSm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">

  <!-- Change class .modal-sm to change the size of the modal -->
  <div class="modal-dialog modal-lg" role="document">


    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title w-100" id="myModalLabel">Biên tập nội dung</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="row">
              <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                  <div class="row">
                      <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                          <p>Mã tag</p>
                      </div>
                      <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                          <p>TAG0001</p>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                          <p>Tên tag</p>
                      </div>
                      <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                          <p>Quảng cáo 1</p>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                          <p>Kích thước </p>
                      </div>
                      <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                          <p>150 - 150</p>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-6 col-sm-6 col-md-6 col-lg-6"></div>
          <form>
          <div class="row">
              <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                  
                 
                  <div class="form-group row">
                    
                    
                    <textarea class="form-control col-lg-11" id="exampleFormControlTextarea1" rows="4" style="width: 80%;margin-left: 17px;border: 1px solid #eee;padding-left: 25px;" placeholder="Trình biên tập nội dung"></textarea>
                  
                  </div>
                  
                  
              </div>
              
          </div>
        </form>
      </div>
      <div class="modal-footer">
        
        <div class="col-6 col-sm-6 col-md-6 col-lg-6">
            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal">Xác nhận</button>
        </div>
        <div class="col-6 col-sm-6 col-md-6 col-lg-6">
             <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
  </div>
</div>
<!-- Central Modal Small -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Thông báo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Thêm mới thành công
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>

<!-- Jquery Core Js --> 


<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Bootstrap JS and jQuery v3.2.1 -->
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- slimscroll, waves Scripts Plugin Js -->  

<script src="plugins/dropzone/dropzone.js"></script> <!-- Dropzone Plugin Js -->
<script src="plugins/momentjs/moment.js"></script> <!-- Moment Plugin Js -->
<script src="plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

<script src="assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js -->
<script>
    $(function () {
    //Datetimepicker plugin
    $('.datetimepicker').bootstrapMaterialDatePicker({
        format: 'dddd DD MMMM YYYY - HH:mm',
        clearButton: true,
        weekStart: 1
    });
});
</script>
</body>

<!-- Mirrored from thememakker.com/templates/oreo/hospital/html/light/index.php by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Apr 2019 15:00:40 GMT -->
</html>