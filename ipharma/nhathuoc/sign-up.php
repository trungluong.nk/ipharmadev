﻿<!doctype html>
<html class="no-js " lang="en">

<!-- Mirrored from thememakker.com/templates/oreo/hospital/html/light/sign-up.php by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Apr 2019 15:01:40 GMT -->
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

    <title>:: Oreo Hospital :: </title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <!-- Custom Css -->
    <link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/authentication.css">
    <link rel="stylesheet" href="assets/css/color_skins.css">
    <style type="text/css">
        .btn-facebook {
            background-color: #3B5998;
            color: white;
        }
        .btn-google {
            background-color: #dd4b39;
            color: white;
        }
        .btn:hover {
          opacity: 0.5;
          color: #fff;
        }
    </style>
</head>

<body class="theme-cyan authentication sidebar-collapse">
<!-- Navbar -->
<nav class="navbar navbar-expand-lg fixed-top navbar-transparent">
    <div class="container">        
        <div class="navbar-translate n_logo">
            <a class="navbar-brand" href="javascript:void(0);" title="" target="_blank"> iPharma</a>
            <button class="navbar-toggler" type="button">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
            </button>
        </div>
        <div class="navbar-collapse">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="index.php">Trang chủ</a>
                </li>
               
                      
                <li class="nav-item">
                    <a class="nav-link btn btn-white btn-round" href="sign-in.php">ĐĂNG NHẬP</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- End Navbar -->
<div class="page-header">
    <div class="page-header-image" style="background-image:url(images/login.jpg)"></div>
    <div class="container">
        <div class="col-md-12 content-center">
            <div class="card-plain">
                <form class="form" method="" action="#">
                    <div class="header">
                        <div class="logo-container">
                            <img src="images/logo.png" alt="" style="margin-top: 0px;">
                        </div>
                       
                        
                    </div>
                    <div class="content">        
                                                               
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Số điện thoại">
                            <span class="input-group-addon">
                                <i class="zmdi zmdi-account-circle"></i>
                            </span>
                        </div>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Email">
                            <span class="input-group-addon">
                                <i class="zmdi zmdi-email"></i>
                            </span>
                        </div>
                        <div class="input-group">
                            <input type="password" placeholder="Mật khẩu" class="form-control" />
                            <span class="input-group-addon">
                                <i class="zmdi zmdi-lock"></i>
                            </span>
                        </div>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Tên">
                            <span class="input-group-addon">
                                
                            </span>
                        </div>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Họ">
                            <span class="input-group-addon">
                                
                            </span>
                        </div>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Nhập thông tin nhà thuốc">
                            <span class="input-group-addon">
                                
                            </span>
                        </div>                      
                    </div>
                    <div class="checkbox" style="margin-bottom: -10px;">
                            <input id="terms" type="checkbox">
                            <label for="terms">
                                    Tôi đã đọc và đồng ý với <a href="javascript:void(0);">Điều khoản và điều khoản trên</a>
                            </label>
                        </div>
                    <div class="footer text-center">
                        <a href="index.php" class="btn btn-primary btn-round btn-lg btn-block waves-effect waves-light">ĐĂNG KÍ</a>
                        
                    </div>
                </form>
            </div>
        </div>
    </div>
    
</div>

<!-- Jquery Core Js -->
<script src="assets/bundles/libscripts.bundle.js"></script>
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
<script>
   $(".navbar-toggler").on('click',function() {
    $("html").toggleClass("nav-open");
});
</script>
</body>

<!-- Mirrored from thememakker.com/templates/oreo/hospital/html/light/sign-up.php by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 25 Apr 2019 15:01:40 GMT -->
</html>